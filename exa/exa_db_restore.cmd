@echo off
set backupFile="C:\dev\viztek\exa\exaweb\RC_exa_pacs_live_latest.backup"
set restoreDb="pacs_live"
set PG_RESTORE="C:\Program Files\PostgreSQL\11\bin\pg_restore.exe"
::set PG_RESTORE="C:\Program Files\PostgreSQL\10\bin\pg_restore.exe"
REM create a listing file
REM %PG_RESTORE% -l %backupFile% > %backupFile%.contents.txt
REM When restoring over existing DB, you can use --clean to drop objects first. 
REM ***DO NOT*** use --clean on empty or newly created DB
set startTime=%time%
%PG_RESTORE%  --host localhost --port 5432 --username "postgres" --password --dbname %restoreDb% --jobs 2 --verbose --exit-on-error %backupFile%
echo start  : %startTime%
echo finish : %time%