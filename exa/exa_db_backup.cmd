::
:: backup an exa postgres database
:: michael.mattsson@konicaminolta.com
::

cls
@echo off
set year=%date:~-4,4%
set month=%date:~-10,2%
set day=%date:~-7,2%
set hourMilitary=%time:~-11,2%
set minute=%time:~-8,2%
set second=%time:~-5,2%
set timestamp=%year%%month%%day%-%hourMilitary%%minute%%second%

set backupFile="RC_exa_pacs_live_%timestamp%.backup"
set dbName="new_pacs_live"
:: set pgDumpCmd="C:\PostgreSQL\9.6\bin\pg_dump.exe"
:: set pgDumpCmd="C:\Program Files\PostgreSQL\10\bin\pg_dump.exe"
set pgDumpCmd="C:\Program Files\PostgreSQL\11\bin\pg_dump.exe"

:: to get the full backup including temp tables,remove following from cmd line:
:: --exclude-table-data temp_* --exclude-table-data audit_log 

set startTime=%time%
%pgDumpCmd% --host localhost --port 5432 --username "postgres" --format custom --password --no-owner --encoding UTF8  --verbose --file %backupFile% %dbName%
echo start  : %startTime%
echo finish : %time%


::
:: upload it to the ftp
::
curl -T %backupFile% ftp://development:1q2w3e4r5t@12.70.252.178//onyxrad/development/michael/db_backup/exa/%backupFile%.backup
curl -v -u development:1q2w3e4r5t ftp://12.70.252.178/onyxrad//onyxrad/development/michael/db_backup/exa/RC_exa_pacs_live_latest.backup -Q "DELE RC_exa_pacs_live_latest.backup"
curl -T %backupFile% ftp://development:1q2w3e4r5t@12.70.252.178//onyxrad/development/michael/db_backup/exa/RC_exa_pacs_live_latest.backup
pause
