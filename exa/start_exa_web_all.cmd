@echo off
@cls

set cwd=%~dp0
for /f "delims=" %%x in (%cwd%../my_config.cfg) do (set "%%x")

:: FORCE script run in admin mode
>nul 2>&1 "%SYSTEMROOT%\system32\cacls.exe" "%SYSTEMROOT%\system32\config\system"

if '%errorlevel%' NEQ '0' (
    echo Requesting Admin access...
    goto goUAC )
    goto goADMIN

:goUAC
    echo Set UAC = CreateObject^("Shell.Application"^) > "%temp%\getadmin.vbs"
    set params = %*:"=""
    echo UAC.ShellExecute "cmd.exe", "/c %~s0 %params%", "", "runas", 1 >> "%temp%\getadmin.vbs"
    "%temp%\getadmin.vbs"
    del "%temp%\getadmin.vbs"
    exit /B

:goADMIN
    pushd "%CD%"
    CD /D "%~dp0"
	
set common_scripts_dir=common
	
::call %common_scripts_dir%\tasks.cmd :START_WIN_SERVICE "redis" "exa_redis_cache"
::call %common_scripts_dir%\tasks.cmd :START_WIN_SERVICE "postgres" "postgresql-x64-10"
::call %common_scripts_dir%\tasks.cmd :START_WIN_TASK "nginx" "nginx.exe" "C:\dev\viztek\exa\exaweb\nginx" "-c C:\dev\viztek\exa\exaweb\cfg\nginx.conf"
::call %common_scripts_dir%\tasks.cmd :START_WIN_TASK_REDIR "exa_web" "node app.js" "C:\dev\viztek\exa\exaweb\web" "env=production port=8080 ssl_port=8443" "C:\dev\viztek\exa\exaweb\log\Web_Out.log"
::TIMEOUT /T 15
::call %common_scripts_dir%\tasks.cmd :START_WIN_TASK_REDIR "exa_web_api" "node app.js" "C:\dev\viztek\exa\exaweb\web" "env=production port=8091 mode=api" "C:\dev\viztek\exa\exaweb\log\Web_Out_APIs.log"

call %common_scripts_dir%\tasks.cmd :START_WIN_TASK_REDIR "exa_web" "npm" "%MY_DEV_ROOT_EXAWEB%/web" "run start" ""
