call %* 
exit /b


:: start a service
::param 1 common name for service
::param 2 service identifier
:START_WIN_SERVICE
SETLOCAL
echo ------------------------
set "srv_name=%~1"
set "srv_id=%~2"
echo "%srv_name%"
for /F "tokens=3 delims=: " %%H in ('sc query "%srv_id%" ^| findstr "        STATE"') do (
  if /I "%%H" NEQ "RUNNING" (
	echo "starting %srv_name%..."
	net start "%srv_id%"
  )else (
	echo "%srv_name% already started."
  )
)
ENDLOCAL
EXIT /B 0



:: stops a service
::param 1 common name for service
::param 2 service identifier
:STOP_WIN_SERVICE
SETLOCAL
echo ------------------------
set "srv_name=%~1"
set "srv_id=%~2"
echo "srv_name=%srv_name%"
echo "srv_id=%srv_id%"
echo "stopping %srv_name%"
for /F "tokens=3 delims=: " %%H in ('sc query "%srv_id%" ^| findstr "        STATE"') do (
  if /I "%%H" EQU "RUNNING" (
	echo "stopping %srv_name%..."
	net stop "%srv_id%" /y
  )else (
	echo "%srv_name% already stopped."
  )
)
ENDLOCAL
EXIT /B 0


:: toggles a service on or off
::param 1 common name for service
::param 2 service identifier
:TOGGLE_WIN_SERVICE
SETLOCAL
set "srv_name=%~1"
set "srv_id=%~2"
net start "%srv_id%" ||NET STOP "%srv_id%" /y
ENDLOCAL
EXIT /B 0


:: starts a running task
::param 1 common name for service
::param 2 service identifier
::param 3 service identifier
::param 4 service identifier
:START_WIN_TASK
SETLOCAL
echo ------------------------
set task_name="%~1"
set task_id=%~2
set task_dir="%~3"
set task_args="%~4"
echo task_name=%task_name%
echo task_id=%task_id%
echo task_dir=%task_dir%
echo task_args=%task_args%
tasklist /nh /fi "imagename eq %task_id%" | find /i "%task_id%" > nul || (
	echo "starting %task_name% processes...."
	cd %task_dir%
	start %task_id% "%task_args%"
)
ENDLOCAL
EXIT /B 0


:: starts a running task
::param 1 common name for service
::param 2 service identifier
::param 3 service identifier
::param 4 service identifier
::param 5 service identifier
:START_WIN_TASK_REDIR
SETLOCAL
echo ------------------------
set task_name="%~1"
set task_id=%~2
set task_dir="%~3"
set task_args="%~4"
set task_output="%~5"
echo task_name=%task_name%
echo task_id=%task_id%
echo task_dir=%task_dir%
echo task_args=%task_args%
echo task_output=%task_output%
tasklist /nh /fi "imagename eq %task_id%" | find /i "%task_id%" > nul || (
	echo "starting %task_name% processes...."
	cd %task_dir%
	echo %task_id% "%task_args%" >> %task_output%
	start %task_id% "%task_args%" >> %task_output%
)
ENDLOCAL
EXIT /B 0


:: stops a running task
::param 1 common name for service
::param 2 service identifier
:STOP_WIN_TASK
SETLOCAL
echo ------------------------
set "task_name=%~1"
set "task_id=%~2"
echo "task_name=%task_name%"
echo "task_id=%task_id%"
echo "stopping %task_name%"
tasklist /nh /fi "imagename eq %task_id%" | find /i "%task_id%" > nul && (
	echo "killing %task_name% processes...."
	taskkill.exe /F /IM %task_id% /T
)
ENDLOCAL
EXIT /B 0
