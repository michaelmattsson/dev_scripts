#!/bin/bash

cd /c/viztek/exa/

if [ -d "/c/viztek/exa/bin" ]; then
	d=$(date +"%Y%m%d_%H%M%S")
	mv /c/viztek/exa/bin /c/viztek/exa/bin_$d
fi
if [ ! -d "/c/viztek/exa/bin" ]; then
	mkdir -p /c/viztek/exa/bin

fi

cd /c/viztek/exa/bin
curl -O -u development:1q2w3e4r5t ftp://12.70.252.178/EXA/Services_Web_Tools_etc/Install_Files/1.4.26/services_1.4.26-rc.3_201906131149.zip
unzip services_1.4.26-rc.3_201906131149.zip -d /c/viztek/exa/bin
