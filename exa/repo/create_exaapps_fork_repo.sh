#!/bin/bash

function pause()
{
	read -p "Press [Enter] key to continue..."
}

function windows_path_to_posix()
{
	echo "$1" | sed -e 's/\\/\//g' -e 's/\([a-zA-Z]\):/\/\1/g'
}

function posix_path_to_windows()
{
	echo "$1" | sed -e 's/^\///' -e 's/\//\\/g' -e 's/^./\0:/'
}

#
# load personal settings & get our posix folders
#
cwd=$(pwd)
parentdir="$(dirname "$(pwd)")"
parentdir="$(dirname "$parentdir")"
. $parentdir/my_config.cfg
DEV_ROOT=$(windows_path_to_posix "$MY_DEV_ROOT")
DEV_ROOT_EXA=$(windows_path_to_posix "$MY_DEV_ROOT_EXA")
DEV_ROOT_EXAAPPS=$(windows_path_to_posix "$MY_DEV_ROOT_EXAAPPS")
DEV_ROOT_EXAWEB=$(windows_path_to_posix "$MY_DEV_ROOT_EXAWEB")
INSTALL_DIR=$(windows_path_to_posix "$MY_INSTALL")
TEMP_DIR=$(windows_path_to_posix "$MY_TEMP_DIR")


function initialize()
{
	clear
	echo "================================="
	echo "==== exaweb development setup ==="
	echo "================================="


	GIT_REPO_NAME=$(basename "$GIT_REPO_EXAWEB_FORK" .git)
	CUR_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
	#TMP_DIR=$(mktemp -d)

	case "$(uname -s)" in

	   Darwin)
		 PLATFORM_OS='OSX'
		 ;;

	   Linux)
		 PLATFORM_OS='LINUX'
		 ;;

	   CYGWIN*|MINGW32*|MINGW64*|MSYS*)
		 PLATFORM_OS='WINDOWS'
		 ;;

	   *)
		 PLATFORM_OS='UNKNOWN' 
		 ;;
	esac

	echo running on $PLATFORM_OS

	#
	# make required dirs
	#
	if [ ! -d "$DEV_ROOT_EXA" ]; then
		echo "creating viztek exa dev folders"
		mkdir -p $DEV_ROOT_EXA
	fi
	cd $DEV_ROOT_EXA

	if [ ! -d "$TEMP_DIR" ]; then
		echo "creating temp folder"
		mkdir -p $TEMP_DIR
	fi

	if [ ! -d "$INSTALL_DIR" ]; then
		echo "creating install folder"
		mkdir -p $INSTALL_DIR
	fi

	echo "placing temporary scripts into $TEMP_DIR"
}







#
# creates an msi install script
#
function install_exe()
{
	local install_file="$1"
	local install_args="$2"
	local install_file_watch="$3"
	local script_name=msi_wrapper.bat
	create_bat_admin_file $script_name
	echo "./$install_file $install_args">> $script_name
	./$script_name
	#wait for installer to finish
	while [ ! -f "$install_file_watch" ]; do sleep 4; done	
	
	. ~/bashrc
	#rm ./$install_file
	#rm ./msi_wrapper.bat
}

#
# creates an msi install script
#
function install_msi()
{
	local install_file="$1"
	local install_args="$2"
	local install_file_watch="$3"
	local script_name=msi_wrapper.bat
	create_bat_admin_file $script_name
	echo "msiexec /i $install_file $install_args">> $script_name
	./$script_name
	#wait for installer to finish
	while [ ! -f "$install_file_watch" ]; do sleep 4; done	
	
	#. ~/bashrc
	#rm ./$install_file
	#rm ./msi_wrapper.bat
}

#
# install git
#
function install_git()
{
	echo "==================================================="
	cd $TEMP_DIR

	case "$PLATFORM_OS" in
		WINDOWS)
			if [ ! -d "/c/Program Files/nodejs/" ] && [ ! -d "/$HOME/nodejs/" ]; then
				echo "git not found!  installing..."
			else
				echo "git already installed."
			fi
		;;
		OSX|LINUX|UNKNOWN)
			if ! [ -x "$(command -v git)" ]; then
				echo "git not found.  installing..."
				sudo yum install -y git
			else
				echo "git already installed."
			fi
		;;
	esac	
}


#
# get new exaapps repo
#
function setup_exaapps_repo()
{
	echo "==================================================="
	echo "downloading current exaapps from repository"
	if [ ! -d "$DEV_ROOT_EXAAPPS" ]; then
		mkdir -p $DEV_ROOT_EXAAPPS
	fi
	cd $DEV_ROOT_EXAAPPS
	git clone $GIT_REPO_EXAAPPS_FORK exaapps

	echo "==================================================="
	echo "adding upstream remote reference and fetching latest upstream commits"
	#read -p "Press [Enter] key to continue..."
	cd $DEV_ROOT_EXAAPPS/exaapps
	git remote add upstream $GIT_REPO_EXAAPPS_UPSTREAM
	git checkout $GIT_REPO_EXAAPPS_BRANCH
	git fetch upstream
	git merge upstream/$GIT_REPO_EXAAPPS_BRANCH
}


#
# get the exaapps 
#
function install_exaapps_files()
{

	local exaapps_install_dir=$(windows_path_to_posix "$MY_EXAAPPS_INSTALL_DIR")


	echo "==================================================="
	if [ ! -d "$exaapps_install_dir/bin" ]; then
		mkdir -p $exaapps_install_dir/bin
		echo created: $exaapps_install_dir/bin
	fi
	if [ ! -d "$exaapps_install_dir/cfg" ]; then
		echo created: $exaapps_install_dir/cfg
		mkdir -p $exaapps_install_dir/cfg
	fi
	
	if [ ! -d "$exaapps_install_dir/resx" ]; then
		echo created: $exaapps_install_dir/resx
		mkdir -p $exaapps_install_dir/resx
	fi
	
	echo "get the resx stuff "
	cd $exaapps_install_dir/resx
	local filename=$(basename "$FTP_EXA_RESX")
	curl -u $FTP_USER:$FTP_PASS ftp://$FTP_HOST/$FTP_EXA_RESX -o $filename
	unzip -o $filename
	rm $filename

	echo "get the exaapps services "
	cd $exaapps_install_dir/bin
	local filename=$(basename "$FTP_EXAAPPS_SERVICES")
	curl -u $FTP_USER:$FTP_PASS ftp://$FTP_HOST/$FTP_EXAAPPS_SERVICES -o $filename
	unzip -o $filename
	rm $filename
				local script_name=services_install.bat
				create_bat_admin_file $script_name
				echo "$exaapps_install_dir/resx/nssm.exe install EXA_Wado_HTTP_Service $exaapps_install_dir/bin/imageserver_http.exe">>  $script_name
				echo "$exaapps_install_dir/resx/nssm.exe set EXA_Wado_HTTP_Service Start SERVICE_DELAYED_AUTO_START">>  $script_name
				echo "$exaapps_install_dir/resx/nssm.exe set EXA_Wado_HTTP_Service DependOnService EXA_Web">>  $script_name

				echo "$exaapps_install_dir/resx/nssm.exe install EXA_DICOM_Service $exaapps_install_dir/bin/dicomservice.exe">>  $script_name
				echo "$exaapps_install_dir/resx/nssm.exe set EXA_DICOM_Service Start SERVICE_DELAYED_AUTO_START">>  $script_name
				echo "$exaapps_install_dir/resx/nssm.exe set EXA_DICOM_Service DependOnService EXA_Web_APIs">>  $script_name

				echo "$exaapps_install_dir/resx/nssm.exe install EXA_Move_Service $exaapps_install_dir/bin/cmoveservice.exe">>  $script_name
				echo "$exaapps_install_dir/resx/nssm.exe set EXA_Move_Service Start SERVICE_DELAYED_AUTO_START">>  $script_name
				echo "$exaapps_install_dir/resx/nssm.exe set EXA_Move_Service DependOnService EXA_Web_APIs">>  $script_name

				echo "$exaapps_install_dir/resx/nssm.exe install EXA_MWL_Service $exaapps_install_dir/bin/mwlservice.exe">>  $script_name
				echo "$exaapps_install_dir/resx/nssm.exe set EXA_MWL_Service Start SERVICE_DELAYED_AUTO_START">>  $script_name
				echo "$exaapps_install_dir/resx/nssm.exe set EXA_MWL_Service DependOnService EXA_Web">>  $script_name

				echo "$exaapps_install_dir/resx/nssm.exe install EXA_Opal_Receive_Service $exaapps_install_dir/bin/opallistenerservice.exe">>  $script_name
				echo "$exaapps_install_dir/resx/nssm.exe set EXA_Opal_Receive_Service Start SERVICE_DELAYED_AUTO_START">>  $script_name
				echo "$exaapps_install_dir/resx/nssm.exe set EXA_Opal_Receive_Service DependOnService EXA_Web_APIs">>  $script_name

				echo "$exaapps_install_dir/resx/nssm.exe install EXA_Opal_Transfer_Service $exaapps_install_dir/bin/opalsenderservice.exe">>  $script_name
				echo "$exaapps_install_dir/resx/nssm.exe set EXA_Opal_Transfer_Service Start SERVICE_DELAYED_AUTO_START">>  $script_name
				echo "$exaapps_install_dir/resx/nssm.exe set EXA_Opal_Transfer_Service DependOnService EXA_Web_APIs">>  $script_name

				echo "$exaapps_install_dir/resx/nssm.exe install EXA_Wado_HTTP_Service $exaapps_install_dir/bin/imageserver_http.exe">>  $script_name
				echo "$exaapps_install_dir/resx/nssm.exe set EXA_Wado_HTTP_Service Start SERVICE_DELAYED_AUTO_START">>  $script_name
				echo "$exaapps_install_dir/resx/nssm.exe set EXA_Wado_HTTP_Service DependOnService EXA_Web">>  $script_name				echo "" >> $script_name
				if [ "$SERVICE_POSTGRES" == "manual" ]; then
					echo ":: CHANGE SERVICE TO MANUAL STARTUP ">>  $script_name
					echo "sc config postgresql-x64-$POSTGRES_MAJ_VERSION start= demand">>  $script_name
					#echo "net stop postgresql-x64-$POSTGRES_MAJ_VERSION">>  $script_name
				fi
				./$script_name
				
				#wait for installer to finish
				while [ ! -f "/c/Program Files/PostgreSQL/$POSTGRES_MAJ_VERSION/uninstall-postgresql.dat" ]; do sleep 4; done





	echo "get the exaapps localcache "
	cd $TEMP_DIR
	filename=$(basename "$FTP_EXAAPPS_LOCALCACHE")
	curl -u $FTP_USER:$FTP_PASS ftp://$FTP_HOST/$FTP_EXAAPPS_LOCALCACHE -o $filename
	install_msi "$filename" "INSTALLDIR=$MY_EXAAPPS_INSTALL_DIR\\localcache /qb" "$exaapps_install_dir/localcache/localcache.exe"
	#msiexec /I exa_localcache_setup.1.4.27.msi INSTALLDIR=d:\viztek\exa\localcache
	rm $filename

	echo "get the exaapps dictation "
	cd $TEMP_DIR
	filename=$(basename "$FTP_EXAAPPS_DICTATION")
	curl -u $FTP_USER:$FTP_PASS ftp://$FTP_HOST/$FTP_EXAAPPS_DICTATION -o $filename
	install_msi "$filename" "INSTALLDIR=$MY_EXAAPPS_INSTALL_DIR\\dictation /qb" "$exaapps_install_dir/dictation/exa_dictation.exe"
	rm $filename
	
	#echo "get the exa client viewer "
	#cd $TEMP_DIR
	#filename=$(basename "$FTP_EXAAPPS_EXACLIENTVIEWER")
	#curl -u $FTP_USER:$FTP_PASS ftp://$FTP_HOST/$FTP_EXAAPPS_EXACLIENTVIEWER -o $filename
	#install_msi "$filename" "/quiet /passive" "OpalViewer.exe"
	#rm $filename
}


initialize
install_git
install_nodejs
move_old_repo
setup_exaapps_repo
install_exaapps_files
