echo off
set script_path=%~dp0
set %script_path:~0,-1%
set nssm_exe=%script_path%..\common\nssm.exe
set exa_install_dir=C:\viztek\exa\1.4.27
set exa_web_install_dir=C:\dev\exa\exaweb_1.4.27
set exa_apps_install_dir=%exa_install_dir%\

goto:main


:wait
PING localhost -n 2 >NUL
goto:eof


:main

echo ---------------------------------
echo - installing REDIS
%exa_web_install_dir%\redis\redis-server.exe --service-install --service-name EXA_Redis_Cache "%exa_web_install_dir%\cfg\redis.windows.conf"
net start "EXA_Redis_Cache"
echo.


echo ---------------------------------
echo - installing NGINX
%nssm_exe% install EXA_Nginx "%exa_web_install_dir%\nginx\nginx.exe"
%nssm_exe% set EXA_Nginx Start SERVICE_AUTO_START
%nssm_exe% set EXA_Nginx Description Exa Web Proxy
%nssm_exe% set EXA_Nginx AppParameters -c %exa_web_install_dir%\cfg\nginx.conf
%nssm_exe% start EXA_Nginx
echo.


echo ---------------------------------
echo - installing EXA_Web
%nssm_exe% install EXA_Web "C:\Program Files\nodejs\node.exe" "%exa_web_install_dir%\web" "app.js env=production port=8080 ssl_port=8443"
%nssm_exe% set EXA_Web Description Exa Web Application
%nssm_exe% start EXA_Web
echo.

echo ---------------------------------
echo - installing EXA_Web_APIs
%nssm_exe% install EXA_Web_APIs "C:\Program Files\nodejs\node.exe" "%exa_web_install_dir%\web\app.js" "env=production port=8091 mode=api"
%nssm_exe% set EXA_Web_APIs Description Exa Web App Interface
%nssm_exe% start EXA_Web_APIs
echo.


echo ---------------------------------
echo - installing EXA_DICOM_Service
%nssm_exe% install EXA_DICOM_Service %exa_install_dir%\bin\dicomservice.exe
%nssm_exe% set EXA_DICOM_Service Start SERVICE_DELAYED_AUTO_START
%nssm_exe% set EXA_DICOM_Service DependOnService EXA_Web_APIs
%nssm_exe% set EXA_DICOM_Service Description Exa DICOM Service
%nssm_exe% start EXA_DICOM_Service
echo.

echo ---------------------------------
echo - installing EXA_Move_Service
%nssm_exe% install EXA_Move_Service %exa_install_dir%\bin\cmoveservice.exe
%nssm_exe% set EXA_Move_Service Start SERVICE_DELAYED_AUTO_START
%nssm_exe% set EXA_Move_Service DependOnService EXA_Web_APIs
%nssm_exe% set EXA_Move_Service Description Exa CMOVE Service
%nssm_exe% start EXA_Move_Service
echo.

echo ---------------------------------
echo - installing EXA_MWL_Service"
%nssm_exe% install EXA_MWL_Service %exa_install_dir%\bin\mwlservice.exe
%nssm_exe% set EXA_MWL_Service Start SERVICE_DEMAND_START
%nssm_exe% set EXA_MWL_Service DependOnService EXA_Web
%nssm_exe% set EXA_MWL_Service Description Exa MWL Service
echo.

echo ---------------------------------
echo - installing EXA_Opal_Receive_Service
%nssm_exe% install EXA_Opal_Receive_Service %exa_install_dir%\bin\opallistenerservice.exe
%nssm_exe% set EXA_Opal_Receive_Service Start SERVICE_DELAYED_AUTO_START
%nssm_exe% set EXA_Opal_Receive_Service DependOnService EXA_Web_APIs
%nssm_exe% set EXA_Opal_Receive_Service Description Exa Opal Recieve Service
%nssm_exe% start EXA_Opal_Receive_Service
echo.


echo ---------------------------------
echo - installing EXA_Opal_Transfer_Service
%nssm_exe% install EXA_Opal_Transfer_Service %exa_install_dir%\bin\opalsenderservice.exe
%nssm_exe% set EXA_Opal_Transfer_Service Start SERVICE_DELAYED_AUTO_START
%nssm_exe% set EXA_Opal_Transfer_Service DependOnService EXA_Web_APIs
%nssm_exe% set EXA_Opal_Transfer_Service Description Exa Opal Transfer Service
%nssm_exe% start EXA_Opal_Transfer_Service
echo.


echo ---------------------------------
echo - installing EXA_Wado_HTTP_Service
%nssm_exe% install EXA_Wado_HTTP_Service %exa_install_dir%\bin\imageserver_http.exe
%nssm_exe% set EXA_Wado_HTTP_Service Start SERVICE_DELAYED_AUTO_START
%nssm_exe% set EXA_Wado_HTTP_Service DependOnService EXA_Web
%nssm_exe% set EXA_Wado_HTTP_Service Description Exa Imageserver Service
%nssm_exe% start EXA_Wado_HTTP_Service
echo.


echo ---------------------------------
echo - installing EXA_HL7_Inbound_Listener
%nssm_exe% install EXA_HL7_Inbound_Listener "C:\Program Files\nodejs\node.exe" "%exa_web_install_dir%\web\app_service.js" "NODE_ENV=SERVICE SERVICE_NAME=HL7R"
%nssm_exe% set EXA_HL7_Inbound_Listener Start SERVICE_DEMAND_START
%nssm_exe% set EXA_HL7_Inbound_Listener Description Exa HL7 Inbound Listener Service
%nssm_exe% set EXA_HL7_Inbound_Listener DependOnService EXA_Web
echo.

echo ---------------------------------
echo - installing EXA_HL7_Inbound_Processor
%nssm_exe% install EXA_HL7_Inbound_Processor "C:\Program Files\nodejs\node.exe" "%exa_web_install_dir%\web\app_service.js" "NODE_ENV=SERVICE SERVICE_NAME=HL7I"
%nssm_exe% set EXA_HL7_Inbound_Processor Start SERVICE_DEMAND_START
%nssm_exe% set EXA_HL7_Inbound_Processor Description Exa HL7 Inbound Processor Service
%nssm_exe% set EXA_HL7_Inbound_Processor DependOnService EXA_Web
echo.

echo ---------------------------------
echo - installing EXA_HL7_Outbound_Processor
%nssm_exe% install EXA_HL7_Outbound_Processor "C:\Program Files\nodejs\node.exe" "%exa_web_install_dir%\web\app_service.js" "NODE_ENV=SERVICE SERVICE_NAME=HL7S"
%nssm_exe% set EXA_HL7_Outbound_Processor Start SERVICE_DEMAND_START
%nssm_exe% set EXA_HL7_Outbound_Processor Description Exa HL7 Outbound Processor Service
%nssm_exe% set EXA_HL7_Outbound_Processor DependOnService EXA_Web
echo.

::%nssm_exe% stop EXA_Dicom_Purger
::%nssm_exe% remove EXA_Dicom_Purger confirm
::call :wait
::%nssm_exe% install EXA_Dicom_Purger "C:\Program Files\nodejs\node.exe" "%exa_web_install_dir%\app_service.js" "NODE_ENV=SERVICE SERVICE_NAME=DP"
::%nssm_exe% set EXA_Dicom_Purger Start SERVICE_DISABLED
::%nssm_exe% set EXA_Dicom_Purger DependOnService EXA_Web

::%nssm_exe% remove EXA_DB_Maintenance confirm
::call :wait
::%nssm_exe% install EXA_DB_Maintenance "C:\Program Files\nodejs\node.exe" "%exa_web_install_dir%\app_service.js" "NODE_ENV=SERVICE SERVICE_NAME=DB"
::%nssm_exe% set EXA_DB_Maintenance Start SERVICE_DISABLED
::%nssm_exe% set EXA_DB_Maintenance DependOnService EXA_Web

echo ---------------------------------
echo - installing EXA_Print_Service
%nssm_exe% install EXA_Print_Service "C:\Program Files\nodejs\node.exe" "%exa_web_install_dir%\web\app_service.js" "NODE_ENV=SERVICE SERVICE_NAME=PRINTSERVICE"
%nssm_exe% set EXA_Print_Service Start SERVICE_DEMAND_START
%nssm_exe% set EXA_Print_Service Description Exa DICOM print Service
%nssm_exe% set EXA_Print_Service DependOnService EXA_Web
echo.

echo ---------------------------------
echo - installing EXA_Appointments_Sync_Service
%nssm_exe% install EXA_Appointments_Sync_Service "C:\Program Files\nodejs\node.exe" "%exa_web_install_dir%\web\app_service.js" "NODE_ENV=SERVICE SERVICE_NAME=APPOINTMENTSSYNC"
%nssm_exe% set EXA_Appointments_Sync_Service Start SERVICE_DEMAND_START
%nssm_exe% set EXA_Appointments_Sync_Service Description Exa Appointments Sync Service
%nssm_exe% set EXA_Appointments_Sync_Service DependOnService EXA_Web
echo.

echo ---------------------------------
echo - installing EXA_SFTP_Sync_Service
%nssm_exe% install EXA_SFTP_Sync_Service "C:\Program Files\nodejs\node.exe" "%exa_web_install_dir%\web\app_service.js" "NODE_ENV=SERVICE SERVICE_NAME=SFTPSYNC"
%nssm_exe% set EXA_SFTP_Sync_Service Start SERVICE_DEMAND_START
%nssm_exe% set EXA_SFTP_Sync_Service Description Exa SFTP Sync Service
%nssm_exe% set EXA_SFTP_Sync_Service DependOnService EXA_Web
echo.

::For New QC2live service
echo ---------------------------------
echo - installing EXA_QC_Service
%nssm_exe% install EXA_QC_Service "C:\Program Files\nodejs\node.exe" "%exa_web_install_dir%\web\pacs_service\qc_to_live\app.js"
%nssm_exe% set EXA_QC_Service Start SERVICE_DELAYED_AUTO_START
%nssm_exe% set EXA_QC_Service DependOnService EXA_Web
%nssm_exe% set EXA_QC_Service Description Exa QC2LIVE Service
%nssm_exe% start EXA_QC_Service
echo.


::billing services
echo ---------------------------------
echo - installing EXA_edi
%nssm_exe% install EXA_edi "%exa_web_install_dir%\exa-edi"
%nssm_exe% set EXA_edi Start SERVICE_DEMAND_START
%nssm_exe% set EXA_edi Application "C:\Program Files\nodejs\npm.cmd"
%nssm_exe% set EXA_edi AppDirectory %exa_web_install_dir%\exa-edi
%nssm_exe% set EXA_edi AppParameters run start-production
%nssm_exe% set EXA_edi Description Exa EDI Service
echo.

echo ---------------------------------
echo - installing EXA_billing
%nssm_exe% install EXA_billing "%exa_web_install_dir%\exa-billing"
%nssm_exe% set EXA_billing Start SERVICE_DEMAND_START
%nssm_exe% set EXA_billing Application "C:\Program Files\nodejs\npm.cmd"
%nssm_exe% set EXA_billing AppDirectory %exa_web_install_dir%\exa-billing
%nssm_exe% set EXA_billing AppParameters run start-production
%nssm_exe% set EXA_billing Description Exa Billing Service
echo.


echo ---------------------------------
echo - installing EXA_SR_Service
%nssm_exe% install EXA_SR_Service "%exa_web_install_dir%\web"
%nssm_exe% set EXA_SR_Service Application "C:\Program Files\nodejs\node.exe"
%nssm_exe% set EXA_SR_Service AppDirectory %exa_web_install_dir%\web
%nssm_exe% set EXA_SR_Service AppParameters app_service.js NODE_ENV=SERVICE SERVICE_NAME=SR
%nssm_exe% set EXA_billing Description Exa Structured Reporting Service
%nssm_exe% start EXA_SR_Service
echo.

::Replace the TxT Service version 23 with version 26
::SC DELETE "TX TextControl 23.0 Service"
::%exa_web_install_dir%\TxTranscription\bin\txregwebsvr.exe /i /e /w

pause
