#!/bin/bash

function pause()
{
	read -p "Press [Enter] key to continue..."
}

function windows_path_to_posix()
{
	echo "$1" | sed -e 's/\\/\//g' -e 's/\([a-zA-Z]\):/\/\1/g'
}

function posix_path_to_windows()
{
	echo "$1" | sed -e 's/^\///' -e 's/\//\\/g' -e 's/^./\0:/'
}

#
# load personal settings & get our posix folders
#
cwd=$(pwd) 	
parentdir="$(dirname "$(pwd)")"
parentdir="$(dirname "$parentdir")"
. $parentdir/my_config.cfg
DEV_ROOT=$(windows_path_to_posix "$MY_DEV_ROOT")
DEV_ROOT_EXA=$(windows_path_to_posix "$MY_DEV_ROOT_EXA")
DEV_ROOT_EXAAPPS=$(windows_path_to_posix "$MY_DEV_ROOT_EXAAPPS")
DEV_ROOT_EXAWEB=$(windows_path_to_posix "$MY_DEV_ROOT_EXAWEB")
INSTALL_DIR=$(windows_path_to_posix "$MY_INSTALL_DIR")
TEMP_DIR=$(windows_path_to_posix "$MY_TEMP_DIR")


function initialize()
{
	clear
	echo "================================="
	echo "==== exaweb development setup ==="
	echo "================================="


	GIT_REPO_NAME=$(basename "$GIT_REPO_EXAWEB_FORK" .git)
	CUR_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
	#TMP_DIR=$(mktemp -d)

	case "$(uname -s)" in

	   Darwin)
		 PLATFORM_OS='OSX'
		 ;;

	   Linux)
		 PLATFORM_OS='LINUX'
		 ;;

	   CYGWIN*|MINGW32*|MINGW64*|MSYS*)
		 PLATFORM_OS='WINDOWS'
		 ;;

	   *)
		 PLATFORM_OS='UNKNOWN' 
		 ;;
	esac

	echo running on $PLATFORM_OS

	#
	# make required dirs
	#
	if [ ! -d "$DEV_ROOT_EXA" ]; then
		echo "creating viztek exa dev folders"
		mkdir -p $DEV_ROOT_EXA
	fi
	cd $DEV_ROOT_EXA

	if [ ! -d "$TEMP_DIR" ]; then
		echo "creating temp folder"
		mkdir -p $TEMP_DIR
	fi

	if [ ! -d "$INSTALL_DIR" ]; then
		echo "creating install folder"
		mkdir -p $INSTALL_DIR
	fi

	echo "placing temporary scripts into $TEMP_DIR"
}




#
# creates a skeleton windows batch file that
# has admin priviledges.  we use this as a template
# when creating batch scripts and append other commands
# to it as needed.
#
function create_bat_admin_file()
{
	local filename=$1
	echo "@echo off" > $filename
	echo "@cls" >> $filename
	echo "" >> $filename
	echo ":: FORCE script run in admin mode" >> $filename
	echo ">nul 2>&1 \"%SYSTEMROOT%\system32\cacls.exe\" \"%SYSTEMROOT%\system32\config\system\"" >> $filename
	echo "" >> $filename
	echo "if '%errorlevel%' NEQ '0' (" >> $filename
	echo "    echo Requesting Admin access..." >> $filename
	echo "    goto goUAC )" >> $filename
	echo "    goto goADMIN" >> $filename
	echo "" >> $filename
	echo ":goUAC" >> $filename
	echo "    echo Set UAC = CreateObject^(\"Shell.Application\"^) > \"%temp%\getadmin.vbs\"" >> $filename
	echo "    set params = %*:\"=\"\"" >> $filename
	echo "    echo UAC.ShellExecute \"cmd.exe\", \"/c %~s0 %params%\", \"\", \"runas\", 1 >> \"%temp%\getadmin.vbs\"" >> $filename
	echo "    \"%temp%\getadmin.vbs\"" >> $filename
	echo "    if exist \"%temp%\getadmin.vbs\NUL\" del \"%temp%\getadmin.vbs\"" >> $filename
	echo "    exit /B" >> $filename
	echo "" >> $filename
	echo ":goADMIN" >> $filename
	echo "    pushd \"%CD%\"" >> $filename
	echo "    CD /D \"%~dp0\"" >> $filename
	echo "" >> $filename
}



function run_windows_admin_command()
{
	cd $TEMP_DIR
	local script_name="$1.bat"
	local script_done_watch="$1.done"
	local command="$2"
	create_bat_admin_file $script_name
	echo "$command">> $script_name
	echo "echo done > $MY_TEMP_DIR/$script_done_watch">> $script_name

	./$script_name
	#wait for installer to finish
	while [ ! -f "$script_done_watch" ]; do sleep 4; done
	rm "$script_done_watch"
}


#
# creates an msi install script
#
function install_exe()
{
	local install_file="$1"
	local install_args="$2"
	local install_file_watch="$3"
	local script_name=msi_wrapper.bat
	create_bat_admin_file $script_name
	echo "./$install_file $install_args">> $script_name
	./$script_name
	#wait for installer to finish
	while [ ! -f "$install_file_watch" ]; do sleep 4; done	
	
	. ~/bashrc
	#rm ./$install_file
	#rm ./msi_wrapper.bat
}

#
# creates an msi install script
#
function install_msi()
{
	local install_file="$1"
	local install_args="$2"
	local install_file_watch="$3"
	local script_name=msi_wrapper.bat
	create_bat_admin_file $script_name
	echo "msiexec /i $install_file $install_args">> $script_name
	./$script_name
	#wait for installer to finish
	while [ ! -f "$install_file_watch" ]; do sleep 4; done	
	
	#. ~/bashrc
	#rm ./$install_file
	#rm ./msi_wrapper.bat
}

#
# install git
#
function install_git()
{
	echo "==================================================="
	cd $TEMP_DIR

	case "$PLATFORM_OS" in
		WINDOWS)
			if [ ! -d "/c/Program Files/nodejs/" ] && [ ! -d "/$HOME/nodejs/" ]; then
				echo "git not found!  installing..."
			else
				echo "git already installed."
			fi
		;;
		OSX|LINUX|UNKNOWN)
			if ! [ -x "$(command -v git)" ]; then
				echo "git not found.  installing..."
				sudo yum install -y git
			else
				echo "git already installed."
			fi
		;;
	esac	
}



#
# install node.js
#
function install_nodejs()
{
	cd $TEMP_DIR
		echo "==================================================="

	case "$PLATFORM_OS" in
		WINDOWS)
			if [ ! -d "/C/Program Files/nodejs/" ] && [ ! -d "/$HOME/nodejs/" ]; then
				echo "node.js not found.  installing..."
				curl https://nodejs.org/dist/v$NODE_VERSION/node-v$NODE_VERSION-x64.msi -o node-v$NODE_VERSION-x64.msi
				install_msi "node-v$NODE_VERSION-x64.msi" "/quiet /passive" "/C/Program Files/nodejs/node.exe"
			else
				echo "node.js already installed."
			fi
		;;
		OSX|LINUX|UNKNOWN)
			if ! [ -x "$(command -v node)" ]; then
				curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.34.0/install.sh | bash
				nvm ls-remote
				nvm install v$NODE_VERSION
				mkdir -p ${HOME}/.npm-packages
				npm config set prefix "${HOME}/.npm-packages"
			else
				echo "node.js already installed."
			fi
		;;
	esac		
}


#
# get new exaweb repo
#
function setup_exaweb_repo()
{
	echo "==================================================="
	echo "downloading current exaweb from repository"
	if [ ! -d "$DEV_ROOT_EXAWEB" ]; then
		mkdir -p $DEV_ROOT_EXAWEB
	fi
	cd $DEV_ROOT_EXAWEB
	git clone --single-branch --branch $GIT_REPO_EXAWEB_BRANCH  $GIT_REPO_EXAWEB_FORK web

	echo "==================================================="
	echo "adding upstream remote reference and fetching latest upstream commits"
	#read -p "Press [Enter] key to continue..."
	cd $DEV_ROOT_EXAWEB/web
	git remote add upstream $GIT_REPO_EXAWEB_UPSTREAM
	git fetch upstream
	git merge upstream/$GIT_REPO_EXAWEB_BRANCH
}


#
# get the exaweb std config & nginx/redis install files 
#
function download_exaweb_cfg_files()
{

	cd $DEV_ROOT_EXAWEB
	echo "==================================================="
	echo "get the exaweb std config & nginx/redis install files "
	#read -p "Press [Enter] key to continue..."

	curl -u $FTP_USER:$FTP_PASS ftp://$FTP_HOST/$FTP_EXAWEB_CFG_FOLDER/exaweb_cfg.zip -o exaweb_cfg.zip
	unzip exaweb_cfg.zip
	rm exaweb_cfg.zip

	curl -u $FTP_USER:$FTP_PASS ftp://$FTP_HOST/$FTP_EXAWEB_CFG_FOLDER/redis_nginx.zip -o redis_nginx.zip
	unzip redis_nginx.zip
	rm redis_nginx.zip
}


function create_web_json_file()
{
	echo "{" >$DEV_ROOT_EXAWEB/cfg/web.json
	echo "  \"dbConnection\": \"tcp://postgres:1q2w3e4r5t@localhost:$POSTGRES_PORT/$POSTGRES_DB\"," >>$DEV_ROOT_EXAWEB/cfg/web.json
	echo "  \"dbConnectionBilling\": \"tcp://postgres:1q2w3e4r5t@localhost:$POSTGRES_PORT/release\",">>$DEV_ROOT_EXAWEB/cfg/web.json
	echo "  \"RedisStore\": {">>$DEV_ROOT_EXAWEB/cfg/web.json
	echo "    \"host\": \"localhost\",">>$DEV_ROOT_EXAWEB/cfg/web.json
	echo "    \"port\": \"6379\"">>$DEV_ROOT_EXAWEB/cfg/web.json
	echo "  },">>$DEV_ROOT_EXAWEB/cfg/web.json
	echo "  \"dbCacheEnabled\": false,">>$DEV_ROOT_EXAWEB/cfg/web.json
	echo "  \"dbCacheTtl\": 30,">>$DEV_ROOT_EXAWEB/cfg/web.json
	echo "   \"jsreport\": {">>$DEV_ROOT_EXAWEB/cfg/web.json
	echo "    \"url\": \"http://localhost/reporting/\",">>$DEV_ROOT_EXAWEB/cfg/web.json
	echo "    \"username\": \"jsradmin\",">>$DEV_ROOT_EXAWEB/cfg/web.json
	echo "    \"password\": \"JSR1q2w3e4r5t\"">>$DEV_ROOT_EXAWEB/cfg/web.json
	echo "  }">>$DEV_ROOT_EXAWEB/cfg/web.json
	echo "}">>$DEV_ROOT_EXAWEB/cfg/web.json
}






#
#install postgres
#
function install_postgres()
{
	echo "==================================================="
	#read -p "Press [Enter] key to continue..."

	#cd $TEMP_DIR
	
	case "$PLATFORM_OS" in
		WINDOWS)
			if [ ! -d "/c/Program Files/PostgreSQL/$POSTGRES_MAJ_VERSION/bin" ] && [ ! -d "/var/lib/postgresql" ]; then
				echo "installing postgres"
				curl https://get.enterprisedb.com/postgresql/postgresql-$POSTGRES_VERSION-windows-x64.exe -o postgresql-$POSTGRES_VERSION-windows-x64.exe

				local script_name=postgres_install.bat
				create_bat_admin_file $script_name
				echo "postgresql-$POSTGRES_VERSION-windows-x64.exe --unattendedmodeui minimal --mode unattended --enable-components pgAdmin --serverport $POSTGRES_PORT --superaccount \"$POSTGRES_USER\" --superpassword \"$POSTGRES_PASS\"" >>  $script_name
				echo "" >> $script_name
				if [ "$SERVICE_POSTGRES" == "manual" ]; then
					echo ":: CHANGE SERVICE TO MANUAL STARTUP ">>  $script_name
					echo "sc config postgresql-x64-$POSTGRES_MAJ_VERSION start= demand">>  $script_name
					#echo "net stop postgresql-x64-$POSTGRES_MAJ_VERSION">>  $script_name
				fi
				./$script_name
				
				#wait for installer to finish
				while [ ! -f "/c/Program Files/PostgreSQL/${POSTGRES_MAJ_VERSION}/uninstall-postgresql.dat" ]; do sleep 4; done
				
			else
				echo "postgres already installed."
			fi
		;;
		OSX|LINUX|UNKNOWN)
			if ! [ -x "$(command -v postgres)" ]; then
				sudo su
				rpm -Uvh https://yum.postgresql.org/$POSTGRES_VERSION/fedora/fedora-30-x86_64/pgdg-fedora-repo-latest.noarch.rpm
				yum install -y postgresql${POSTGRES_MAJ_VERSION}-server
				/usr/pgsql-${POSTGRES_MAJ_VERSION}/bin/postgresql-${POSTGRES_MAJ_VERSION}-setup initdb
				systemctl enable postgresql-${POSTGRES_MAJ_VERSION}.service
				systemctl start postgresql-${POSTGRES_MAJ_VERSION}.service
				/usr/pgsql-${POSTGRES_MAJ_VERSION}/bin/postgresql-${POSTGRES_MAJ_VERSION}-setup initdb
				sudo -u postgres psql --command '\password postgres'
				
				#wait for installer to finish
				#while [ ! -f "/c/Program Files/PostgreSQL/$POSTGRES_MAJ_VERSION/uninstall-postgresql.dat" ]; do sleep 4; done
				#curl https://get.enterprisedb.com/postgresql/postgresql-$POSTGRES_VERSION-linux-x64.run -o #postgresql-$POSTGRES_VERSION-linux-x64.run
			else
				echo "postgres already installed."
			fi
		;;
	esac
}

#
#install python
#
function install_python()
{
	echo "==================================================="
	#read -p "Press [Enter] key to continue..."

	#cd $TEMP_DIR
	
	case "$PLATFORM_OS" in
		WINDOWS)
			if [ ! -f "/c/Python27/LICENSE.txt" ] && [ ! -d "/var/lib/python" ]; then
				echo "installing postgres"
				curl https://www.python.org/ftp/python/2.7.16/python-2.7.16.amd64.msi -o python-2.7.16.amd64.msi

				local script_name=python_install.bat
				create_bat_admin_file $script_name
				echo "python-2.7.16.amd64.msi /qn /norestart" >>  $script_name
				echo "" >> $script_name
				./$script_name
				
				#wait for installer to finish
				while [ ! -f "/c/Python27/LICENSE.txt" ]; do sleep 4; done
				
			else
				echo "python already installed."
			fi
		;;
		OSX|LINUX|UNKNOWN)
			echo "python already installed."
		;;
	esac
}


#
# install redis
#
function install_redis()
{
	echo "==================================================="
	echo "install redis"
	cd $TEMP_DIR

	case "$PLATFORM_OS" in
		WINDOWS)
			local script_name=redis_install.bat
			create_bat_admin_file $script_name
			
			echo "cd /d $MY_DEV_ROOT_EXAWEB/redis">> $script_name
			echo "redis-server.exe --service-install --service-name EXA_Redis_Cache $MY_DEV_ROOT_EXAWEB/cfg/redis.windows.conf" >> $script_name
			if [ "$SERVICE_REDIS" == "manual" ]; then
				echo "sc config EXA_Redis_Cache start= demand">> $script_name
			fi
			echo "net start EXA_Redis_Cache">> $script_name
			./$script_name
		;;
		OSX|LINUX|UNKNOWN)
			sudo yum install -y redis
			mkdir -p ${HOME}/.npm-packages
			npm config set prefix "${HOME}/.npm-packages"
		;;
	esac		


}



#
# stop redis
#
function stop_redis()
{
	echo "==================================================="
	echo "stopping redis"
	cd $TEMP_DIR

	case "$PLATFORM_OS" in
		WINDOWS)
			local script_name=stop_redis.bat
			create_bat_admin_file $script_name
			echo "net stop exa_redis_cache /y">> $script_name
			./$script_name
		;;
		OSX|LINUX|UNKNOWN)
			echo "not implemented in $PLATFORM_OS"
		;;
	esac		
}

#
# start redis
#
function start_redis()
{
	echo "==================================================="
	echo "starting redis"
	cd $TEMP_DIR

	case "$PLATFORM_OS" in
		WINDOWS)
			local script_name=start_redis.bat
			create_bat_admin_file $script_name
			echo "net start exa_redis_cache /y">> $script_name
			./$script_name
		;;
		OSX|LINUX|UNKNOWN)
			echo "not implemented in $PLATFORM_OS"
		;;
	esac		
}



#
# start_nginx nginx
#
function start_nginx()
{
	echo "==================================================="
	echo "starting nginx"
	cd $TEMP_DIR

	case "$PLATFORM_OS" in
		WINDOWS)
			local script_name=start_nginx.bat
			create_bat_admin_file $script_name
			

			echo "set task_id=nginx.exe">> $script_name
			echo "set task_name=nginx">> $script_name
			echo "set task_dir=\"$MY_DEV_ROOT_EXAWEB/nginx\"">> $script_name
			echo "set task_args=\"-c $MY_DEV_ROOT_EXAWEB/cfg/nginx.conf\"">> $script_name
			echo "tasklist /nh /fi \"imagename eq %task_id%\" | find /i \"%task_id%\" > nul || (">> $script_name
			echo "	echo \"starting %task_name% processes....\"">> $script_name
			echo "	cd /d %task_dir%">> $script_name
			echo "	start %task_id% %task_args%">> $script_name
			echo ")">> $script_name
			./$script_name
		;;
		OSX|LINUX|UNKNOWN)
			echo "not implemented in $PLATFORM_OS"
		;;
	esac		

}
#
# stop nginx
#
function stop_nginx()
{
	echo "==================================================="
	echo "stopping nginx"
	cd $TEMP_DIR

	case "$PLATFORM_OS" in
		WINDOWS)
			local script_name=stop_nginx.bat
			create_bat_admin_file $script_name
			
			echo "set task_id=nginx.exe">> $script_name
			echo "tasklist /nh /fi \"imagename eq %task_id%\" | find /i \"%task_id%\" > nul && (">> $script_name
			echo "	echo \"killing %task_name% processes....\"">> $script_name
			echo "	taskkill.exe /F /IM %task_id% /T">> $script_name
			echo ")">> $script_name
			./$script_name
		;;
		OSX|LINUX|UNKNOWN)
			echo "not implemented in $PLATFORM_OS"
		;;
	esac		
}




#
# start postgres
#
function start_postgres()
{
	echo "==================================================="
	echo "starting postgres"
	cd $TEMP_DIR

	case "$PLATFORM_OS" in
		WINDOWS)
			local script_name=start_postgres.bat
			create_bat_admin_file $script_name
			echo "net start postgresql-x64-$POSTGRES_MAJ_VERSION /y">> $script_name
			./$script_name
		;;
		OSX|LINUX|UNKNOWN)
		;;
	esac		
}

#
# stop postgres
#
function stop_postgres()
{
	echo "==================================================="
	echo "stopping postgres"
	cd $TEMP_DIR
	case "$PLATFORM_OS" in
		WINDOWS)
			local script_name=stop_postgres.bat
			create_bat_admin_file $script_name
			echo "net stop postgresql-x64-$POSTGRES_MAJ_VERSION /y">> $script_name
			./$script_name
		;;
		OSX|LINUX|UNKNOWN)
			echo "not implemented in $PLATFORM_OS"
		;;
	esac		
}



function create_pgpass_file()
{

	echo "localhost:$POSTGRES_PORT:$POSTGRES_DB:$POSTGRES_USER:$POSTGRES_PASS" > ~/.pgpass
	case "$PLATFORM_OS" in
		WINDOWS)
		local script_name=exa_create_pgpass.bat
		create_bat_admin_file $script_name
		echo "md %APPDATA%\postgresql">> $script_name
		echo "echo localhost:$POSTGRES_PORT:postgres:$POSTGRES_USER:$POSTGRES_PASS> %APPDATA%\postgresql\pgpass.conf">> $script_name
		echo "echo localhost:$POSTGRES_PORT:$POSTGRES_DB:$POSTGRES_USER:$POSTGRES_PASS>> %APPDATA%\postgresql\pgpass.conf">> $script_name
		./$script_name
		;;
	esac	
}


#
# kill all db connections
#
function kill_db_connections()
{
	#kill any connections first
	local sql_script="SELECT pg_terminate_backend (pid) FROM pg_stat_activity WHERE datname = \'$POSTGRES_DB\';" 
	execute_sql_call "$POSTGRES_DB" "kill_db_connections" "$sql_script"
}

#
# if $POSTGRES_DB exists, then rename it
#
function rename_current_db()
{
	kill_db_connections

	local d=$(date +"%Y%m%d_%H%M%S")
	local new_db_name="$POSTGRES_DB_$d"
	local sql_script="ALTER DATABASE $POSTGRES_DB RENAME TO $new_db_name;"
	execute_sql_call "$POSTGRES_DB" "rename_db" "$sql_script"
}


function restore_backup_database()
{
	echo restoring sample database to postgres started
	echo "localhost:$POSTGRES_PORT:$POSTGRES_DB:$POSTGRES_USER:$POSTGRES_PASS" > ~/.pgpass

	cd $TEMP_DIR
	
	case "$PLATFORM_OS" in
		WINDOWS)
			local script_name=exa_db_restore.bat
			create_bat_admin_file $script_name
			
			echo "set script_path=%~dp0">> $script_name
			echo "echo %script_path:~0,-1%">> $script_name

			echo "set backup_file=\"$MY_TEMP_DIR/$FTP_DB_BACKUP_FILE\"">> $script_name
			echo "set pg_dbname=$POSTGRES_DB">> $script_name
			echo "set pg_username=$POSTGRES_USER">> $script_name
			echo "set pg_pass=$POSTGRES_PASS">> $script_name
			echo "set pg_create_db=\"C:/Program Files/PostgreSQL/$POSTGRES_MAJ_VERSION/bin/createdb.exe\"">> $script_name
			echo "set pg_create_user=\"C:/Program Files/PostgreSQL/$POSTGRES_MAJ_VERSION/bin/createuser.exe\"">> $script_name
			echo "set pg_restore=\"C:/Program Files/PostgreSQL/$POSTGRES_MAJ_VERSION/bin/pg_restore.exe\"">>$script_name
			echo "set pg_cli=\"C:/Program Files/PostgreSQL/$POSTGRES_MAJ_VERSION/bin/psql.exe\"">> $script_name
			echo "">> $script_name
			echo "set startTime=%time%">> $script_name
			#echo "%pg_create_db% -U %pg_username% --no-password --maintenance-db=postgres %pg_dbname%">> $script_name
			echo "">> $script_name
			#echo "echo CREATE DATABASE pacs_live WITH OWNER = postgres ENCODING = 'UTF8' CONNECTION LIMIT = -1; >%script_path%\create_user.sql">> $script_name
			#echo "echo DROP SCHEMA public; >%script_path%/create_user.sql">> $script_name
			#echo "echo CREATE USER exa_billing WITH ENCRYPTED PASSWORD '%pg_pass%'; >>%script_path%/create_user.sql">> $script_name
			#echo "echo GRANT ALL PRIVILEGES ON DATABASE %pg_dbname% TO exa_billing; >>%script_path%/create_user.sql">> $script_name
			#echo "%pg_cli% -h localhost -p $POSTGRES_PORT -U %pg_username% -d %pg_dbname% -f %script_path%/create_user.sql">> $script_name
			#echo "">> $script_name
			echo "%pg_restore% --host localhost --port $POSTGRES_PORT --username=%pg_username% --no-password --dbname %pg_dbname% --jobs 2 --verbose --exit-on-error %backup_file%">> $script_name
			echo "">> $script_name
			echo "echo start  : %startTime%">> $script_name
			echo "echo finish : %time%">> $script_name
			echo "echo done > $MY_TEMP_DIR/exa_db_restore.done">> $script_name

			./$script_name
			while [ ! -f "$TEMP_DIR/exa_db_restore.done" ]; do sleep 4; done
			rm $TEMP_DIR/exa_db_restore.done
		;;
		OSX|LINUX|UNKNOWN)
			echo "not implemented in $PLATFORM_OS"
		;;
	esac	
	
	echo restoring sample database to postgres finished

}


#
# get sample db backup from ftp and restore
#
function download_test_db()
{
	echo "==================================================="
	echo "get sample db backup from ftp and restore"
	cd $cwd
	curl -u $FTP_USER:$FTP_PASS ftp://$FTP_HOST/$FTP_DB_BACKUP_FOLDER/$FTP_DB_BACKUP_FILE -o $TEMP_DIR/$FTP_DB_BACKUP_FILE
}


function execute_sql_call()
{
	local sql_connect_db=$1
	local sql_function_name=$2
	local sql_call="$3"
	echo "==================================================="
	echo "launching sql function $sql_function_name"

	case "$PLATFORM_OS" in
		WINDOWS)
			local script_name=$sql_function_name.bat
			local sql_script_name=$sql_function_name.sql
			local done_file=$sql_function_name.done
			cd $TEMP_DIR
			
			echo $sql_call > $sql_script_name
			
			create_bat_admin_file $script_name
			echo "cd /d \"C:/Program Files/PostgreSQL/$POSTGRES_MAJ_VERSION/bin\"" >> $script_name
			echo "psql.exe -h localhost -p $POSTGRES_PORT -U $POSTGRES_USER -f \"$MY_TEMP_DIR/$sql_script_name\" $sql_connect_db" >> $script_name
			echo "echo done > $MY_TEMP_DIR/$done_file">> $script_name
			./$script_name
			while [ ! -f "$TEMP_DIR/$done_file" ]; do sleep 4; done
			rm $TEMP_DIR/$done_file
		;;
		OSX|LINUX|UNKNOWN)
			echo "not implemented in $PLATFORM_OS"
		;;
	esac	
	
}


#
# create postgres database
#
function create_shell_db()
{
	echo "==================================================="
	echo "creating $POSTGRES_DB database"

local sql_script="CREATE DATABASE $POSTGRES_DB \
WITH \
OWNER = postgres \
ENCODING = 'UTF8' \
LC_COLLATE = 'English_United States.1252' \
LC_CTYPE = 'English_United States.1252' \
TABLESPACE = pg_default \
CONNECTION LIMIT = -1;"

	execute_sql_call "postgres" "create_shell_db" "$sql_script"
}

function create_public_schema()
{
	echo "==================================================="
	echo "creating public schema on $POSTGRES_DB database"

local sql_script="CREATE SCHEMA public \
    AUTHORIZATION postgres; \
 \
COMMENT ON SCHEMA public \
    IS 'standard public schema'; \
 \
GRANT ALL ON SCHEMA public TO postgres; \
 \
GRANT ALL ON SCHEMA public TO PUBLIC; \

ALTER DEFAULT PRIVILEGES IN SCHEMA public \
GRANT ALL ON TABLES TO exa_billing; \
 \
ALTER DEFAULT PRIVILEGES IN SCHEMA public \
GRANT ALL ON SEQUENCES TO exa_billing; \
 \
ALTER DEFAULT PRIVILEGES IN SCHEMA public \
GRANT EXECUTE ON FUNCTIONS TO exa_billing;"

	execute_sql_call "$POSTGRES_DB" "create_public_schema" "$sql_script"
}

function create_extensions_schema()
{
	echo "==================================================="
	echo "creating extensions schema on $POSTGRES_DB database"

local sql_script="CREATE SCHEMA extensions \
    AUTHORIZATION postgres; \
 \
COMMENT ON SCHEMA extensions \
    IS 'Dedicated schema for extensions'; \
 \
GRANT ALL ON SCHEMA extensions TO exa_billing; \
 \
GRANT ALL ON SCHEMA extensions TO postgres; \
 \
ALTER DEFAULT PRIVILEGES IN SCHEMA extensions \
GRANT EXECUTE ON FUNCTIONS TO exa_billing;"

	execute_sql_call "$POSTGRES_DB" "create_extensions_schema" "$sql_script"
}

function create_chat_schema()
{
	echo "==================================================="
	echo "creating chat schema on $POSTGRES_DB database"

local sql_script="CREATE SCHEMA chat \
    AUTHORIZATION postgres; \
 \
COMMENT ON SCHEMA chat \
    IS 'ExaChat Service';"

	execute_sql_call "$POSTGRES_DB" "create_chat_schema" "$sql_script"
}

function create_billing_schema()
{
	echo "==================================================="
	echo "creating chat schema on $POSTGRES_DB database"

local sql_script="CREATE SCHEMA billing \
    AUTHORIZATION exa_billing; \
 \
COMMENT ON SCHEMA billing \
    IS 'Schema for EXA Billing related DB objects'; \
 \
GRANT ALL ON SCHEMA billing TO exa_billing;"

	execute_sql_call "$POSTGRES_DB" "create_billing_schema" "$sql_script"
}



#
# create postgres database
#
function add_roles()
{
	echo "==================================================="
	echo "creating exa_billing role"

local sql_script="CREATE USER exa_billing WITH \
NOLOGIN \
NOSUPERUSER \
INHERIT \
NOCREATEDB \
NOCREATEROLE \
NOREPLICATION;"

	execute_sql_call "$POSTGRES_DB" "create_roles" "$sql_script"
}


#
# alter postgres database
#
function alter_db()
{
	echo "==================================================="
	echo "need to add a few things to the postgres database"

local sql_script="ALTER DATABASE $POSTGRES_DB \
SET search_path TO public, extensions;"

	execute_sql_call "$POSTGRES_DB" "alter_db" "$sql_script"
}


# run npm installs
function run_npm_installs()
{
	if [ -d "$DEV_ROOT_EXAWEB/web" ]; then
		echo "==================================================="
		echo "run npm installs"
		cd "$DEV_ROOT_EXAWEB/web"
		npm install
		npm install -g grunt-cli
		npm install -g grunt
		#npm install --save libxslt@git+https://github.com/alexdee2007/node-libxslt.git
		npm run styles
		npm run update_db
	fi
}

exaweb_dry_run()
{
		cd "$DEV_ROOT_EXAWEB/web"
		npm run start
}



#
# exaweb needs F drive
# does f:\ drive exist? if not then create a link to c:\viztek\f_drive
#
function create_f_virtual_drive()
{
	if [ ! -d /f/ ]; then
		echo "==================================================="
		echo "creating virtual f drive"

		case "$PLATFORM_OS" in
			WINDOWS)
				cd $TEMP_DIR
				local script_name=create_f_drive_link.bat
				create_bat_admin_file $script_name

				echo "" >> $script_name
				echo "if not exist $MY_INSTALL_DIR\\f_drive (" >> $script_name
				echo "	mkdir -p $MY_INSTALL_DIR\\f_drive" >> $script_name
				echo ") " >> $script_name
				echo "" >> $script_name
				echo "if not exist f:\\ (" >> $script_name
				echo "	subst f: $MY_INSTALL_DIR\\f_drive" >> $script_name
				echo "	REG ADD \"HKCU\\Software\\Microsoft\\Windows\\CurrentVersion\\Run\" /v /f \"F Drive\" /t REG_SZ /d \"subst F: $MY_INSTALL_DIR\\f_drive\"" >> $script_name
				echo ") " >> $script_name
				./$script_name
			;;
			OSX|LINUX|UNKNOWN)
				echo "not implemented in $PLATFORM_OS"
			;;
		esac	
	else
		echo "==================================================="
		echo "f drive already exists."
	fi
}


function cleanup()
{
	echo "==================================================="
	echo "cleaning up temporary folder $TEMP_DIR"
	rm -rf $TEMP_DIR
}


#
# move old repo if it exists
#
function move_old_repo()
{
	if [ -d "$DEV_ROOT_EXAWEB" ]; then
		#we're moving old install so we need to stop some services first
		stop_redis
		#since we're moving the web folder, we need to uninstall redis
		run_windows_admin_command "uninstall_redis" "$MY_DEV_ROOT_EXAWEB/redis/redis-server.exe --service-uninstall"
		stop_nginx
		stop_postgres
		sleep 3s
		echo "renaming previous exaweb folder"
		#read -p "Press [Enter] key to continue..."
		d=$(date +"%Y%m%d_%H%M%S")
		local new_folder_name="$MY_DEV_ROOT_EXAWEB"_"$d"
		echo moving "$MY_DEV_ROOT_EXAWEB" to "$new_folder_name"
		run_windows_admin_command "move_old_repo" "mv $MY_DEV_ROOT_EXAWEB $new_folder_name"
		if [ -d "$new_folder_name" ]; then
			echo moved successfully
		else
			echo move failed.
			exit 1
		fi
		
		
		
	fi
}


function set_webapps_registry()
{
	local exaapps_install_dir=$(windows_path_to_posix "$MY_EXAAPPS_INSTALL_DIR")
	if [ ! -d "$exaapps_install_dir/resx" ]; then
		echo "get the resx stuff "
		echo created: $exaapps_install_dir/resx
		mkdir -p $exaapps_install_dir/resx
		cd $exaapps_install_dir/resx
		local filename=$(basename "$FTP_EXA_RESX")
		curl -u $FTP_USER:$FTP_PASS ftp://$FTP_HOST/$FTP_EXA_RESX -o $filename
		unzip -o $filename
		rm $filename
	fi
	
	if [ -f "$exaapps_install_dir/resx/nssm.exe" ]; then
		cd $TEMP_DIR
		local script_name=web_registry.bat
		create_bat_admin_file $script_name

		local servicemode_auto="SERVICE_AUTO_START"
		local servicemode_auto_delayed="SERVICE_DELAYED_AUTO_START"
		if [ "$SERVICE_EXAWEB" == "manual" ]; then
			servicemode_auto="SERVICE_DEMAND_START"
			servicemode_auto_delayed="SERVICE_DEMAND_START"
		fi

		echo "$MY_EXAAPPS_INSTALL_DIR\\resx\\nssm.exe install EXA_Web \"C:\\Program Files\\nodejs\\node.exe\" \"$MY_DEV_ROOT_EXAWEB\\web\" \"app.js env=production port=8080 ssl_port=8443\" ">>$script_name
		echo "$MY_EXAAPPS_INSTALL_DIR\\resx\\nssm.exe set EXA_Web Start $servicemode_auto ">>$script_name

		echo "$MY_EXAAPPS_INSTALL_DIR\\resx\\nssm.exe install EXA_Web_APIs \"C:\\Program Files\\nodejs\\node.exe\" \"$MY_DEV_ROOT_EXAWEB\\web\\app.js\" \"env=production port=8091 mode=api\" ">>$script_name
		echo "$MY_EXAAPPS_INSTALL_DIR\\resx\\nssm.exe set EXA_Web_APIs Start $servicemode_auto ">>$script_name

		echo "$MY_EXAAPPS_INSTALL_DIR\\resx\\nssm.exe install EXA_HL7_Inbound_Listener \"C:\\Program Files\\nodejs\\node.exe\" \"$MY_DEV_ROOT_EXAWEB\\web\\app_service.js\" \"NODE_ENV=SERVICE SERVICE_NAME=HL7R\" ">>$script_name
		echo "$MY_EXAAPPS_INSTALL_DIR\\resx\\nssm.exe set EXA_HL7_Inbound_Listener Start $servicemode_auto_delayed ">>$script_name
		echo "$MY_EXAAPPS_INSTALL_DIR\\resx\\nssm.exe set EXA_HL7_Inbound_Listener DependOnService EXA_Web ">>$script_name

		echo "$MY_EXAAPPS_INSTALL_DIR\\resx\\nssm.exe install EXA_HL7_Inbound_Processor \"C:\\Program Files\\nodejs\\node.exe\" \"$MY_DEV_ROOT_EXAWEB\\web\\app_service.js\" \"NODE_ENV=SERVICE SERVICE_NAME=HL7I\" ">>$script_name
		echo "$MY_EXAAPPS_INSTALL_DIR\\resx\\nssm.exe set EXA_HL7_Inbound_Processor Start $servicemode_auto_delayed ">>$script_name
		echo "$MY_EXAAPPS_INSTALL_DIR\\resx\\nssm.exe set EXA_HL7_Inbound_Processor DependOnService EXA_Web ">>$script_name

		echo "$MY_EXAAPPS_INSTALL_DIR\\resx\\nssm.exe install EXA_HL7_Outbound_Processor \"C:\\Program Files\\nodejs\\node.exe\" \"$MY_DEV_ROOT_EXAWEB\\web\\app_service.js\" \"NODE_ENV=SERVICE SERVICE_NAME=HL7S\" ">>$script_name
		echo "$MY_EXAAPPS_INSTALL_DIR\\resx\\nssm.exe set EXA_HL7_Outbound_Processor Start $servicemode_auto_delayed ">>$script_name
		echo "$MY_EXAAPPS_INSTALL_DIR\\resx\\nssm.exe set EXA_HL7_Outbound_Processor DependOnService EXA_Web ">>$script_name

		#echo "$MY_EXAAPPS_INSTALL_DIR\\resx\\nssm.exe install EXA_Dicom_Purger \"C:\\Program Files\\nodejs\\node.exe\" \"$MY_DEV_ROOT_EXAWEB\\web\\app_service.js\" \"NODE_ENV=SERVICE SERVICE_NAME=DP\" ">>$script_name
		#echo "$MY_EXAAPPS_INSTALL_DIR\\resx\\nssm.exe set EXA_Dicom_Purger Start SERVICE_DISABLED ">>$script_name
		#echo "$MY_EXAAPPS_INSTALL_DIR\\resx\\nssm.exe set EXA_Dicom_Purger DependOnService EXA_Web ">>$script_name

		#echo "$MY_EXAAPPS_INSTALL_DIR\\resx\\nssm.exe install EXA_DB_Maintenance \"C:\\Program Files\\nodejs\\node.exe\" \"$MY_DEV_ROOT_EXAWEB\\web\\app_service.js\" \"NODE_ENV=SERVICE SERVICE_NAME=DB\" ">>$script_name
		#echo "$MY_EXAAPPS_INSTALL_DIR\\resx\\nssm.exe set EXA_DB_Maintenance Start SERVICE_DISABLED ">>$script_name
		#echo "$MY_EXAAPPS_INSTALL_DIR\\resx\\nssm.exe set EXA_DB_Maintenance DependOnService EXA_Web ">>$script_name

		echo "$MY_EXAAPPS_INSTALL_DIR\\resx\\nssm.exe install EXA_Print_Service \"C:\\Program Files\\nodejs\\node.exe\" \"$MY_DEV_ROOT_EXAWEB\\web\\app_service.js\" \"NODE_ENV=SERVICE SERVICE_NAME=PRINTSERVICE\" ">>$script_name
		echo "$MY_EXAAPPS_INSTALL_DIR\\resx\\nssm.exe set EXA_Print_Service Start $servicemode_auto_delayed ">>$script_name
		echo "$MY_EXAAPPS_INSTALL_DIR\\resx\\nssm.exe set EXA_Print_Service DependOnService EXA_Web ">>$script_name

		echo "$MY_EXAAPPS_INSTALL_DIR\\resx\\nssm.exe install EXA_Appointments_Sync_Service \"C:\\Program Files\\nodejs\\node.exe\" \"$MY_DEV_ROOT_EXAWEB\\web\\app_service.js\" \"NODE_ENV=SERVICE SERVICE_NAME=APPOINTMENTSSYNC\" ">>$script_name
		echo "$MY_EXAAPPS_INSTALL_DIR\\resx\\nssm.exe set EXA_Appointments_Sync_Service Start $servicemode_auto_delayed ">>$script_name
		echo "$MY_EXAAPPS_INSTALL_DIR\\resx\\nssm.exe set EXA_Appointments_Sync_Service DependOnService EXA_Web ">>$script_name

		echo "$MY_EXAAPPS_INSTALL_DIR\\resx\\nssm.exe install EXA_SFTP_Sync_Service \"C:\\Program Files\\nodejs\\node.exe\" \"$MY_DEV_ROOT_EXAWEB\\web\\app_service.js\" \"NODE_ENV=SERVICE SERVICE_NAME=SFTPSYNC\" ">>$script_name
		echo "$MY_EXAAPPS_INSTALL_DIR\\resx\\nssm.exe set EXA_SFTP_Sync_Service Start $servicemode_auto_delayed ">>$script_name
		echo "$MY_EXAAPPS_INSTALL_DIR\\resx\\nssm.exe set EXA_SFTP_Sync_Service DependOnService EXA_Web ">>$script_name

		#For New QC2live service
		echo "$MY_EXAAPPS_INSTALL_DIR\\resx\\nssm.exe install EXA_QC_Service \"C:\\Program Files\\nodejs\\node.exe\" \"$MY_DEV_ROOT_EXAWEB\\web\\pacs_service\\qc_to_live\\app.js\" ">>$script_name
		echo "$MY_EXAAPPS_INSTALL_DIR\\resx\\nssm.exe set EXA_QC_Service Start $servicemode_auto_delayed ">>$script_name
		echo "$MY_EXAAPPS_INSTALL_DIR\\resx\\nssm.exe set EXA_QC_Service DependOnService EXA_Web ">>$script_name

		#billing services
		echo "$MY_EXAAPPS_INSTALL_DIR\\resx\\nssm install EXA_edi \"$MY_DEV_ROOT_EXAWEB\\exa-edi\" ">>$script_name
		echo "$MY_EXAAPPS_INSTALL_DIR\\resx\\nssm.exe set EXA_edi Start $servicemode_auto ">>$script_name
		echo "$MY_EXAAPPS_INSTALL_DIR\\resx\\nssm set EXA_edi Application \"C:\\Program Files\\nodejs\\npm.cmd\" ">>$script_name
		echo "$MY_EXAAPPS_INSTALL_DIR\\resx\\nssm set EXA_edi AppDirectory $MY_DEV_ROOT_EXAWEB\\exa-edi ">>$script_name
		echo "$MY_EXAAPPS_INSTALL_DIR\\resx\\nssm set EXA_edi AppParameters run start-production ">>$script_name

		echo "$MY_EXAAPPS_INSTALL_DIR\\resx\\nssm install EXA_billing \"$MY_DEV_ROOT_EXAWEB\\exa-billing\" ">>$script_name
		echo "$MY_EXAAPPS_INSTALL_DIR\\resx\\nssm.exe set EXA_billing Start $servicemode_auto ">>$script_name
		echo "$MY_EXAAPPS_INSTALL_DIR\\resx\\nssm set EXA_billing Application \"C:\\Program Files\\nodejs\\npm.cmd\" ">>$script_name
		echo "$MY_EXAAPPS_INSTALL_DIR\\resx\\nssm set EXA_billing AppDirectory $MY_DEV_ROOT_EXAWEB\\exa-billing ">>$script_name
		echo "$MY_EXAAPPS_INSTALL_DIR\\resx\\nssm set EXA_billing AppParameters run start-production ">>$script_name

		#Cardio Module Service (For Exa.1.4.26 and up)
		echo "$MY_EXAAPPS_INSTALL_DIR\\resx\\nssm install EXA_SR_Service \"$MY_DEV_ROOT_EXAWEB\\web\" ">>$script_name
		echo "$MY_EXAAPPS_INSTALL_DIR\\resx\\nssm.exe set EXA_SR_Service Start $servicemode_auto ">>$script_name
		echo "$MY_EXAAPPS_INSTALL_DIR\\resx\\nssm set EXA_SR_Service Application \"C:\\Program Files\\nodejs\\node.exe\" ">>$script_name
		echo "$MY_EXAAPPS_INSTALL_DIR\\resx\\nssm set EXA_SR_Service AppDirectory $MY_DEV_ROOT_EXAWEB\\web ">>$script_name
		echo "$MY_EXAAPPS_INSTALL_DIR\\resx\\nssm set EXA_SR_Service AppParameters app_service.js NODE_ENV=SERVICE SERVICE_NAME=SR ">>$script_name
		./$script_name
	fi
}



# TODO: doesnt work.
function create_exaweb_shortcut()
{
	@echo off
	cd $TEMP_DIR
	case "$PLATFORM_OS" in
		WINDOWS)
			local script_name="create_exaweb_shortcut.vbs"
			echo "Set oWS = WScript.CreateObject(\"WScript.Shell\")" >> $script_name
			echo "sLinkFile = \"%USERPROFILE%\Desktop\exaweb_start.lnk\"">> $script_name
			echo "Set oLink = oWS.CreateShortcut(sLinkFile)" >> $script_name
			echo "oLink.TargetPath = \"$MY_DEV_ROOT_EXAWEB\npm.exe\"" >> $script_name
			echo "oLink.Arguments=\"run start\"" >> $script_name
			echo "oLink.WorkingDirectory=\"$MY_DEV_ROOT_EXAWEB\"" >> $script_name
			echo "oLink.Save" >> $script_name

			cscript /nologo $script_name
			del $script_name
			;;
		OSX|LINUX|UNKNOWN)
			echo "create exaweb shortcut not implemented for $PLATFORM_OS yet."
		;;
	esac
}


function main()
{
	initialize
	install_git
	install_nodejs
	move_old_repo
	setup_exaweb_repo
	download_exaweb_cfg_files
	create_web_json_file
	install_redis

	create_pgpass_file
	install_postgres
	start_postgres
	#install_python
	download_test_db
	create_shell_db
	add_roles
	create_public_schema
	create_extensions_schema
	create_chat_schema
	create_billing_schema
	rename_current_db
	restore_backup_database
	alter_db


	run_npm_installs
	create_f_virtual_drive
	start_nginx
	start_redis
 	set_webapps_registry
	cleanup


	exaweb_dry_run
	##create_exaweb_shortcut
}

main

echo "done.  goodbye."

