echo off
set script_path=%~dp0
set %script_path:~0,-1%
set nssm_exe=%script_path%..\common\nssm.exe
set exa_install_dir=C:\viztek\exa\1.4.27
set exa_web_install_dir=C:\dev\exa\exaweb_1.4.27
set exa_apps_install_dir=%exa_install_dir%\

goto:main


:wait
PING localhost -n 2 >NUL
goto:eof


:main


echo ---------------------------------
echo - removing NGINX
%nssm_exe% stop EXA_Nginx
%nssm_exe% remove EXA_Nginx confirm
echo.


echo ---------------------------------
echo - removing EXA_Web
net stop "EXA_Web"
%nssm_exe% stop EXA_Web
wait
%nssm_exe% remove EXA_Web confirm
sc delete "EXA_Web"
echo.

echo ---------------------------------
echo - removing EXA_Web_APIs
net stop "EXA_Web"
%nssm_exe% stop EXA_Web_APIs
wait
%nssm_exe% remove EXA_Web_APIs confirm
sc delete "EXA_Web"
echo.


echo ---------------------------------
echo - removing EXA_DICOM_Service
%nssm_exe% stop EXA_DICOM_Service
%nssm_exe% remove EXA_DICOM_Service confirm
echo.

echo ---------------------------------
echo - removing EXA_Move_Service
%nssm_exe% stop EXA_Move_Service
%nssm_exe% remove EXA_Move_Service confirm
echo.

echo ---------------------------------
echo - removing EXA_MWL_Service"
%nssm_exe% stop EXA_MWL_Service
%nssm_exe% remove EXA_MWL_Service confirm
echo.

echo ---------------------------------
echo - removing EXA_Opal_Receive_Service
%nssm_exe% stop EXA_Opal_Receive_Service
%nssm_exe% remove EXA_Opal_Receive_Service confirm
echo.


echo ---------------------------------
echo - removing EXA_Opal_Transfer_Service
%nssm_exe% stop EXA_Opal_Transfer_Service
%nssm_exe% remove EXA_Opal_Transfer_Service confirm
echo.


echo ---------------------------------
echo - removing EXA_Wado_HTTP_Service
%nssm_exe% stop EXA_Wado_HTTP_Service
%nssm_exe% remove EXA_Wado_HTTP_Service confirm
echo.


echo ---------------------------------
echo - removing EXA_HL7_Inbound_Listener
%nssm_exe% stop EXA_HL7_Inbound_Listener
%nssm_exe% remove EXA_HL7_Inbound_Listener confirm
echo.

echo ---------------------------------
echo - removing EXA_HL7_Inbound_Processor
%nssm_exe% stop EXA_HL7_Inbound_Processor
%nssm_exe% remove EXA_HL7_Inbound_Processor confirm
echo.

echo ---------------------------------
echo - removing EXA_HL7_Outbound_Processor
%nssm_exe% stop EXA_HL7_Outbound_Processor
%nssm_exe% remove EXA_HL7_Outbound_Processor confirm
echo.

::%nssm_exe% stop EXA_Dicom_Purger
::%nssm_exe% remove EXA_Dicom_Purger confirm
::call :wait
::%nssm_exe% install EXA_Dicom_Purger "C:\Program Files\nodejs\node.exe" "%exa_web_install_dir%\app_service.js" "NODE_ENV=SERVICE SERVICE_NAME=DP"
::%nssm_exe% set EXA_Dicom_Purger Start SERVICE_DISABLED
::%nssm_exe% set EXA_Dicom_Purger DependOnService EXA_Web

::%nssm_exe% remove EXA_DB_Maintenance confirm
::call :wait
::%nssm_exe% install EXA_DB_Maintenance "C:\Program Files\nodejs\node.exe" "%exa_web_install_dir%\app_service.js" "NODE_ENV=SERVICE SERVICE_NAME=DB"
::%nssm_exe% set EXA_DB_Maintenance Start SERVICE_DISABLED
::%nssm_exe% set EXA_DB_Maintenance DependOnService EXA_Web

echo ---------------------------------
echo - removing EXA_Print_Service
%nssm_exe% stop EXA_Print_Service
%nssm_exe% remove EXA_Print_Service confirm
echo.

echo ---------------------------------
echo - removing EXA_Appointments_Sync_Service
%nssm_exe% stop EXA_Appointments_Sync_Service
%nssm_exe% remove EXA_Appointments_Sync_Service confirm
echo.

echo ---------------------------------
echo - removing EXA_SFTP_Sync_Service
%nssm_exe% stop EXA_SFTP_Sync_Service
%nssm_exe% remove EXA_SFTP_Sync_Service confirm
echo.

::For New QC2live service
echo ---------------------------------
echo - removing EXA_QC_Service
%nssm_exe% stop EXA_QC_Service
%nssm_exe% remove EXA_QC_Service confirm
echo.


::billing services
echo ---------------------------------
echo - removing EXA_edi
%nssm_exe% stop EXA_edi
%nssm_exe% remove EXA_edi confirm
echo.

echo ---------------------------------
echo - removing EXA_billing
%nssm_exe% stop EXA_billing
%nssm_exe% remove EXA_billing confirm
echo.


echo ---------------------------------
echo - removing EXA_SR_Service
%nssm_exe% stop EXA_SR_Service
%nssm_exe% remove EXA_SR_Service confirm
echo.

::Replace the TxT Service version 23 with version 26
::SC DELETE "TX TextControl 23.0 Service"
::%exa_web_install_dir%\TxTranscription\bin\txregwebsvr.exe /i /e /w


echo ---------------------------------
echo - removing REDIS
net stop EXA_Redis_Cache
%exa_web_install_dir%\redis\redis-server.exe --service-uninstall
sc delete "EXA_Redis_Cache"
echo.


pause
