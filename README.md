# Scripts that do stuff...
***michael.mattsson@konicaminolta.com***  

- this is a collection of scripts that I`ve created to make repetative tasks easier for my complicated life (ie. a collection of scripts that do stuff)  

### YOU MUST
**in the scripts root directory, you need to edit the file "my_config.cfg" and change the repo variables to point to your own pre-existing forks, as well as the MY_DEV_ROOT_XXXX variables which point to your local drive and should reflect where you want the dev files to be installed to.

### Database stuff
**exa/exa_db_backup.cmd**;
windows shell script that does a simple backup of your current postgres pacs_live database and stores it locally.;
**exa/exa_db_restore.cmd**;
windows shell script that does a simple restore of your current postgres pacs_live database from a local file.;

**database/db_exa_backup.bat**;
windows shell script that backs up a local postgress database, appends a timestamp to the filename and uploads it to the ftp;
**database/db_exa_restore.bat**;
windows shell script retrieves all backups found on the ftp, presents the list to the user console screen and allows the user to choose which backup to restore to the local postgres database.   If there is already a pacs_live database, the script will rename the local database before restoring.;
**database/db_opal_backup.bat**;
windows shell script that backups the opal sql server database locally.;
**database/db_opal_restore.bat**;
windows shell script that restores a previously backup .;


### Services stuff
**exa/start_cmoveservice.cmd**  
windows shell script that starts the exa cmove service  
**exa/start_dicomservice.cmd**  
windows shell script that starts the exa cmove service  
**exa/start_exa_services.cmd**  
windows shell script that starts all the exa apps service  
**exa/start_exa_web.cmd**  
windows shell script that starts the exa web service  
**exa/start_exa_web_all.cmd**  
windows shell script that starts the exa web apps  
**exa/start_exa_web_api.cmd**  
windows shell script that starts the exa web api app  
**exa/start_exa_web_nginx.cmd**  
windows shell script that starts the exa nginx service  
**exa/start_exa_web_redis.cmd**  
windows shell script that starts the exa redis service  
**exa/start_imageserver_http.cmd**  
windows shell script that starts the exa wado imageserver service  
**exa/start_opallistenerservice.cmd**  
windows shell script that starts the exa opal listener service  
**exa/start_opalsenderservice.cmd**  
windows shell script that starts the exa opal send service  
**exa/start_postgres.cmd**  
windows shell script that starts postgres database  

**exa/stop_cmoveservice.cmd**  
windows shell script that stops the exa cmove service  
**exa/stop_dicomservice.cmd**  
windows shell script that stops the exa dicom service  
**exa/stop_exa_services.cmd**  
windows shell script that stops all exaapps services  
**exa/stop_exa_web.cmd**  
windows shell script that stops the exaweb app  
**exa/stop_exa_web_all.cmd**  
windows shell script that stops the exaweb apps/services  
**exa/stop_exa_web_api.cmd**  
windows shell script that stops the exaweb api  
**exa/stop_exa_web_nginx.cmd**  
windows shell script that stops the nginx service  
**exa/stop_exa_web_redis.cmd**  
windows shell script that stops the redis service  
**exa/stop_imageserver.cmd**  
windows shell script that stops the wado imageserver service  
**exa/stop_opallistenerservice.cmd**  
windows shell script that stops the opal listener service  
**exa/stop_opalsenderservice.cmd**  
windows shell script that stops the exa opal send service  
**exa/stop_postgres.cmd**  
windows shell script that stops the postgres database  

# Misc stuff
**exa/common/tasks.cmd**  
common utils & stuff used by other scripts - don't worry about it!  

### Dev setup stuff
**exa/repo/create_exaapps_fork_repo.sh**  
bash script that sets up a new development environment for exaapps development.  

**exa/repo/create_exaweb_fork_repo.sh**  
bash script that sets up a new development environment for exaweb development.  
-it stops the postgres service (if running)  
-it stops the redis service (if running)  
-it stops the nginx service (if running)  
-it renames any current local exaweb repository (if any)  
-downloads, installs and configures the latest redis (inside the new exaweb folder)  
-downloads, installs and configures the latest nginx (inside the new exaweb folder)  
-downloads, installs npm and required libraries (inside the new exaweb folder)  
-grabs the latest development branch of exaweb, and configures  
-restarts the redis, nginx & postgres services  
-runs run styles, update_db etc...  
-runs exaweb and exawebapi.  
