::
:: simple backup script for exa postgres database
:: backs up current pacs_live database, stores it on the ftp
::
:: michael.mattsson@konicaminolta.com
::
cls
@echo off

cwd=$(pwd) 	
. my_config.cfg

set year=%date:~-4,4%
set month=%date:~-10,2%
set day=%date:~-7,2%
set hourMilitary=%time:~-11,2%
set minute=%time:~-8,2%
set second=%time:~-5,2%
set timestamp=%year%%month%%day%-%hourMilitary%%minute%%second%

set EXAWEB_VERSION=1.4.27

set POSTGRES_MAJ_VERSION=12
set POSTGRES_VERSION=12.1-3
set POSTGRES_USER=postgres
set POSTGRES_PASS=1q2w3e4r5t
set POSTGRES_PORT=5432
set POSTGRES_DB=pacs_live

set FTP_HOST=12.70.252.178
set FTP_USER=development
set FTP_PASS=1q2w3e4r5t
set FTP_EXAWEB_CFG_FOLDER=onyxrad/development/michael/exaweb
set FTP_DB_BACKUP_FOLDER=onyxrad/development/michael/db_backup/exa
set FTP_EXA_RESX=onyxrad/development/michael/exa/dev_setup
set FTP_DB_BACKUP_FILE=RC_exa_pacs_live_latest_12.backup




for /f "tokens=1-2 delims=:" %%a in ('ipconfig^|find "IPv4"') do set ip=%%b
set my_ip=%ip:~1%



if exist "C:\PostgreSQL\9.6\bin\pg_dump.exe" (
	set pg_backup_exe="C:\PostgreSQL\9.6\bin\pg_dump.exe"
	set pg_ver=9.6
)
if exist "C:\Program Files\PostgreSQL\9.6\bin\pg_dump.exe" (
	set pg_backup_exe="C:\Program Files\PostgreSQL\9.6\bin\pg_dump.exe"
	set pg_ver=9.6
)
if exist "C:\Program Files\PostgreSQL\10\bin\pg_dump.exe" (
	set pg_backup_exe="C:\Program Files\PostgreSQL\10\bin\pg_dump.exe"
	set pg_ver=10
)
if exist "C:\Program Files\PostgreSQL\11\bin\pg_dump.exe" (
	set pg_backup_exe="C:\Program Files\PostgreSQL\11\bin\pg_dump.exe"
	set pg_ver=11
)
if exist "C:\Program Files\PostgreSQL\12\bin\pg_dump.exe" (
	set pg_backup_exe="C:\Program Files\PostgreSQL\12\bin\pg_dump.exe"
	set pg_ver=12
)

set backup_file="RC_exa_pacs_live_%EXAWEB_VERSION%_%pg_ver%_%my_ip%_%timestamp%.backup"
set db_name="pacs_live"

if not exist %userprofile%\.pgpass (
	echo localhost:%POSTGRES_PORT%:pacs_live:%POSTGRES_USER%:%POSTGRES_PASS%  > %userprofile%\.pgpass
)
echo start dump  : %time%
%pg_backup_exe% --host localhost --port %POSTGRES_PORT% --username "%POSTGRES_USER%" --format custom --no-owner --encoding UTF8  --verbose --file "%TEMP%\\%backup_file%" %db_name%
if %ERRORLEVEL% NEQ 0 (
	echo "error while trying to backup postgres database!"
	goto :finished
)

echo finish dump : %time%


::
:: upload it to the ftp
::

echo start ftp send  : %time%
curl -T %TEMP%\\%backup_file% ftp://development:1q2w3e4r5t@12.70.252.178/%FTP_DB_BACKUP_FOLDER%/%backup_file%
echo ERRORLEVEL = %ERRORLEVEL%
if %ERRORLEVEL% NEQ 0 (
	echo "error while trying to upload to ftp!"
	goto :finished
) else (
    del /Q %TEMP%\\%backup_file%
)
echo finish ftp send : %time%

:finished
echo that's all folks!