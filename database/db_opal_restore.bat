echo off
cls
echo -- RESTORE DATABASE --
setlocal enabledelayedexpansion

set archive_folder=%CD%\backups
set extract_folder=%CD%\tmp
set target_date=
set db_name=
set db_filename=
set target_archive=
set database_folder=

goto:main


:show_menu
	set Index=1
	pushd %archive_folder%
	for %%f in (*.*) do (
	  set "archives[!Index!]=%%f"
	  set /a Index+=1
	)
	set /a UBound=Index-1
	popd

	for /l %%i in (1,1,%UBound%) do echo %%i. !archives[%%i]!
	:choiceloop
	set /p Choice=Your choice: 
	if "%Choice%"=="" goto chioceloop
	if %Choice% LSS 1 goto choiceloop
	if %Choice% GTR %UBound% goto choiceloop
	set target_archive=!archives[%Choice%]!
	set target_date=%target_archive:~12,-4%
	::echo target_archive = %target_archive%
	::echo target_date = %target_date%
goto:EOF


:unzip_archive
	set source=%~1
	set target=%~2
	set backup_filename=%CD%\backups\%source%
	echo call 7za e "%backup_filename%" -o%target%
	call 7za x "%backup_filename%" -o%target%
goto:EOF



:restore_db
	set folder=%~1
	set fname=%~2
	set dbname=%fname:~0,-4%
	set server_name=%COMPUTERNAME%
	set user_name=sa
	set password=1q2w3e4r5t
	
	echo folder=%folder%
	echo fname=%fname%
	echo dbname=%dbname%
	echo server_name=%server_name%
	echo user_name=%user_name%
	echo password=%password%
	
	sqlcmd -U %user_name% -P %password% -S %server_name% -d master -Q "ALTER DATABASE [%dbname%] SET SINGLE_USER WITH ROLLBACK IMMEDIATE"

	:: WARNING - delete the database, suits me
	:: sqlcmd -E -S %server_name% -d master -Q "IF EXISTS (SELECT * FROM sysdatabases WHERE name=N'%dbname%' ) DROP DATABASE [%dbname%]"
	:: sqlcmd -E -S %server_name% -d master -Q "CREATE DATABASE [%dbname%]"

	:: restore
	sqlcmd -U %user_name% -P %password% -S %server_name% -d master -Q "RESTORE DATABASE [%dbname%] FROM DISK = N'%folder%\%fname%' WITH REPLACE"

	:: remap user/login (http://msdn.microsoft.com/en-us/library/ms174378.aspx)
	sqlcmd -U %user_name% -P %password% -S %server_name% -d %dbname% -Q "sp_change_users_login 'Update_One', 'login-name', 'user-name'"
	sqlcmd -U %user_name% -P %password% -S %server_name% -d master -Q "ALTER DATABASE [%dbname%] SET MULTI_USER"

goto:EOF


:find_extracted_dbs
	set db_tmp_folder=%~1
	echo db_tmp_folder="%db_tmp_folder%"
	pushd "%db_tmp_folder%"
	echo %CD%
	for %%f in (*.bak) do (
		call :restore_db %db_tmp_folder% %%f
	)
	popd
goto:EOF
	



:main
call :show_menu
md "%extract_folder%"
echo 
call :unzip_archive "%target_archive%" "%extract_folder%"
call :find_extracted_dbs %extract_folder%\%target_date%
if exist "%extract_folder%\*" rmdir /S /Q "%extract_folder%"



