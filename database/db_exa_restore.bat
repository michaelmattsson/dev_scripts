::
:: simple restore script for exa postgres database
:: gets list of available postgres database backuops from the ftp, 
:: presents the user with a list, then based of user selection, it renames any current pacs_live database and restores
:: the backup from the ftp to the local postgres
::
:: michael.mattsson@konicaminolta.com
::
@echo off
cls
echo -- RESTORE DATABASE --
setlocal enabledelayedexpansion

set archive_folder=%CD%\backups
set extract_folder=%CD%\test
set target_date=
set target_archive=
set postgres_bin_dir=

set ftp_folder_list_file=.\available_backups.txt


set cwd=%~dp0
for /f "delims=" %%x in (%cwd%../my_config.cfg) do (set "%%x")


for /f "tokens=1-2 delims=:" %%a in ('ipconfig^|find "IPv4"') do set ip=%%b
set my_ip=%ip:~1%


goto:main


:find_latest_postgres_install
if exist "C:\PostgreSQL\9.6\bin\pg_restore.exe" (
set postgres_bin_dir="C:\PostgreSQL\9.6\bin"
set pg_ver=9.6
)
if exist "C:\Program Files\PostgreSQL\9.6\bin\pg_restore.exe" (
set postgres_bin_dir="C:\Program Files\PostgreSQL\9.6\bin"
set pg_ver=9.6
)
if exist "C:\Program Files\PostgreSQL\10\bin\pg_restore.exe" (
set postgres_bin_dir="C:\Program Files\PostgreSQL\10\bin"
set pg_ver=10
)
if exist "C:\Program Files\PostgreSQL\11\bin\pg_restore.exe" (
set postgres_bin_dir="C:\Program Files\PostgreSQL\11\bin"
set pg_ver=11
)
if exist "C:\Program Files\PostgreSQL\12\bin\pg_restore.exe" (
set postgres_bin_dir="C:\Program Files\PostgreSQL\12\bin"
set pg_ver=12
)
if exist "C:\Program Files\PostgreSQL\13\bin\pg_restore.exe" (
set postgres_bin_dir="C:\Program Files\PostgreSQL\13\bin"
set pg_ver=13
)
if exist "C:\Program Files\PostgreSQL\14\bin\pg_restore.exe" (
set postgres_bin_dir="C:\Program Files\PostgreSQL\14\bin"
set pg_ver=14
)
if exist "C:\Program Files\PostgreSQL\15\bin\pg_restore.exe" (
set postgres_bin_dir="C:\Program Files\PostgreSQL\15\bin"
set pg_ver=15
)

	echo latest postgres bin dir found: %postgres_bin_dir%
goto:EOF


#
# creates a skeleton windows batch file that
# has admin priviledges.  we use this as a template
# when creating batch scripts and append other commands
# to it as needed.
#
:create_bat_admin_file
	set filename=%~1
	echo @echo off> %filename%
	echo @cls>> %filename%
	echo.  >>%filename%
	echo :: FORCE script run in admin mode>> %filename%
	echo ^>nul 2^>^&1 "%%SYSTEMROOT%%\system32\cacls.exe" "%%SYSTEMROOT%%\system32\config\system" >> %filename%
	echo.  >> %filename%
	echo if '%%errorlevel%%' NEQ '0' (>> %filename%
	echo     echo Requesting Admin access...>> %filename%
	echo     goto goUAC ^) >> %filename%
	echo     goto goADMIN >> %filename%
	echo.  >> %filename%
	echo :goUAC >> %filename%
	echo     echo set UAC=CreateObject^("Shell.Application"^) ^> "%extract_folder%\getadmin.vbs" >> %filename%
	::echo     set params=%%*:"=" >> %filename%
	::echo     echo UAC.ShellExecute "cmd.exe", "/c %%~s0 %%params%%", "", "runas", 1 ^>^> "%extract_folder%\\getadmin.vbs" >> %filename%
	echo     set params=%*
	echo     set params=%params:"=\"% >> %filename%
	echo     echo UAC.ShellExecute "cmd.exe", "/C %%~s0", "%params%", "runas", 1 ^>^> "%extract_folder%\\getadmin.vbs" >> %filename%
	echo     "%extract_folder%\\getadmin.vbs" >> %filename%
	echo     del "%extract_folder%\\getadmin.vbs" >> %filename%
	echo     exit /B >> %filename%
	echo.  >> %filename%
	echo :goADMIN >> %filename%
	echo     pushd "%%CD%%" >> %filename%
	echo     CD /D "%%~dp0" >> %filename%
	echo.  >> %filename%
goto:EOF







:run_postgres_restore
	echo "run_postgres_restore....."
	set backup_file=%~1
	set pgbindir=%postgres_bin_dir:"=%
	::echo localhost:%POSTGRES_PORT%:%POSTGRES_DB%:%POSTGRES_USER%:%POSTGRES_PASS% > ~/.pgpass

	set script_name="%extract_folder%/tmp_exa_db_restore.bat"
	call :create_bat_admin_file %script_name%


	echo admin bat file created
	echo set "script_path=%extract_folder%" >> %script_name%
	echo set "timestamp=%%date:~-4,4%%%%date:~-10,2%%%%date:~-7,2%%_%%time:~0,2%%%%time:~3,2%%" >> %script_name%
	
	
	::echo echo %script_path:~0,-1%>> %script_name%

	::echo "set backupFile=\"%script_path%RC_exa_pacs_latest.backup\"">> %script_name%
	echo set "backup_file=%backup_file%">> %script_name%
	echo set "pg_dbname=%POSTGRES_DB%">> %script_name%
	echo set "pg_port=%POSTGRES_PORT%">> %script_name%
	echo set "pg_username=%POSTGRES_USER%">> %script_name%
	echo set "pg_pass=%POSTGRES_PASS%">> %script_name%
	echo set "pg_create_db=%pgbindir%\createdb.exe">> %script_name%
	echo set "pg_create_user=%pgbindir%\createuser.exe">> %script_name%
	echo set "pg_restore=%pgbindir%\pg_restore.exe">>%script_name%
	echo set "pg_cli=%pgbindir%\psql.exe">> %script_name%
	echo. >> %script_name%
	echo if not exist %APPDATA%\postgresql mkdir %APPDATA%\postgresql>> %script_name%
	echo echo localhost:%%pg_port%%:postgres:%%pg_username%%:%%pg_pass%%^>%%APPDATA%%\postgresql\pgpass.conf>> %script_name%
	echo echo localhost:%%pg_port%%:%%pg_dbname%%:%%pg_username%%:%%pg_pass%%^>^>%%APPDATA%%\postgresql\pgpass.conf>> %script_name%
	echo. >> %script_name%
	echo set start_time=%%time%% >> %script_name%
	echo echo renaming old %POSTGRES_DB% database...>> %script_name%
	echo echo ALTER DATABASE %%pg_dbname%% RENAME TO %%pg_dbname%%_%%timestamp%%; ^>%%script_path%%\pg_restore_script_1.sql >> %script_name%
	echo "%%pg_cli%%" -h localhost -p %%pg_port%% -U %%pg_username%% -d postgres -f %%script_path%%\pg_restore_script_1.sql>> %script_name%
	echo. >> %script_name%
	echo echo creating new database template...>> %script_name%
	echo "%%pg_create_db%%" -U %%pg_username%% --no-password --maintenance-db=postgres %%pg_dbname%% >> %script_name%
	echo. >> %script_name%
	
	echo echo creating billing user...>> %script_name%
	echo echo CREATE USER exa_billing WITH ENCRYPTED PASSWORD '%%pg_pass%%'; ^>^>%%script_path%%\pg_restore_script_2.sql >> %script_name%
	echo echo GRANT ALL PRIVILEGES ON DATABASE %%pg_dbname%% TO exa_billing; ^>^>%%script_path%%\pg_restore_script_2.sql >> %script_name%
	echo "%%pg_cli%%" -h localhost -p %%pg_port%% -U %%pg_username%% -d postgres -f pg_restore_script_2.sql>> %script_name%
	echo. >> %script_name%
	echo echo restoring database from backup...>> %script_name%
	echo "%%pg_restore%%" --host localhost --port %%pg_port%% --username=%%pg_username%% --no-password --dbname %%pg_dbname%% --jobs 2 --verbose --exit-on-error %%backup_file%% >> %script_name%
	echo. >> %script_name%
	echo echo start  : %%start_time%% >> %script_name%
	echo echo finish : %%time%% >> %script_name%

	call %script_name%
	echo "run_postgres_restore.....DONE!"
goto:EOF



:find_available_backups
	> ft.do echo Open %FTP_HOST%
	>> ft.do echo %FTP_USER%
	>> ft.do echo %FTP_PASS%
	>> ft.do echo prompt n
	>> ft.do echo bin
	>> ft.do echo cd %FTP_DB_BACKUP_FOLDER%
	>> ft.do echo ls -h *.backup %ftp_folder_list_file%
	>> ft.do echo bye
	ftp -s:ft.do
	del ft.do
goto:EOF


:show_menu_ftp
	call :find_available_backups
	for /F "delims=" %%a in (%ftp_folder_list_file%) do (
			set /A count+=1
			set "archives[!count!]=%%a"
	)
	
	del %ftp_folder_list_file%

	:show_menu_ftp_choiceloop
	::cls
	echo backups available:
	echo.
	echo 0. EXIT
	for /L %%i in (1,1,%count%) do echo %%i. !archives[%%i]!
	echo.
	set /p Choice=Your choice: 
	if "%Choice%"=="" call :show_menu_ftp_choiceloop
	if %Choice% LSS 1 goto :exit_script
	if %Choice% GTR %count% call :show_menu_ftp_choiceloop
	set target_archive=!archives[%Choice%]!
	set target_date=%target_archive:~17,-7%
	set target_pg_version=%target_archive:~17,-7%
	set target_host=%target_archive:~17,-7%
	echo target_archive = %target_archive%
	echo target_host = %target_host%
	echo target_pg_version = %target_pg_version%
	echo target_date = %target_date%
	
	
	call :download_from_ftp %extract_folder%
	::call :unzip_archive "%target_archive%"
	::call :restore_db "%target_archive%"
	call :run_postgres_restore "%extract_folder%\%target_archive%"
	call :exit_script
goto:EOF



:download_from_ftp
	md "%~1"
	> ft.do echo Open %FTP_HOST%
	>> ft.do echo %FTP_USER%
	>> ft.do echo %FTP_PASS%
	>> ft.do echo prompt n
	>> ft.do echo bin
	>> ft.do echo lcd %~1
	>> ft.do echo cd %FTP_DB_BACKUP_FOLDER%
	>> ft.do echo get %target_archive%
	>> ft.do echo bye
	ftp -s:ft.do
	del ft.do
goto:EOF


:show_menu_local
	set Index=1
	pushd %archive_folder%
	for %%f in (*.*) do (
	  set "archives[!Index!]=%%f"
	  set /a Index+=1
	)
	set /a UBound=Index-1
	popd

	for /l %%i in (1,1,%UBound%) do echo %%i. !archives[%%i]!
	:show_menu_local_choiceloop
	set /p Choice=Your choice: 
	if "%Choice%"=="" call :show_menu_local_choiceloop
	if %Choice% LSS 1 goto :exit_script
	if %Choice% GTR %UBound% call :show_menu_local_choiceloop
	set target_archive=!archives[%Choice%]!
	set target_pg_version=%target_archive:~12,-4%
	set target_host=%target_archive:~12,-4%
	set target_date=%target_archive:~12,-4%
	echo target_archive = %target_archive%
	echo target_pg_version = %target_pg_version%
	echo target_host = %target_host%
	echo target_date = %target_date%
goto:EOF


:unzip_archive
	set source=%~1
	set target=%~2
	set backup_filename=%CD%\backups\%source%
	echo call 7za e "%backup_filename%" -o%target%
	call 7za x "%backup_filename%" -o%target%
goto:EOF



:restore_db
	set fname=%~1
	set folder=tmp
	set dbname=%fname:~0,-4%
	set server_name="localhost"

	echo fname=%fname%
	echo dbname=%dbname%
	
if not exist %userprofile%\.pgpass (
	echo localhost:%POSTGRES_PORT%:%POSTGRES_DB%:%POSTGRES_USER%:%POSTGRES_PASS%  > %userprofile%\.pgpass
)

set pg_restore_exe="%postgres_bin_dir%\pg_restore.exe"


	REM create a listing file
	REM %POSTGRES_DB% -l %backupFile% > %backupFile%.contents.txt
	REM When restoring over existing DB, you can use --clean to drop objects first. 
	REM ***DO NOT*** use --clean on empty or newly created DB
	set startTime=%time%
	%pg_restore_exe%  --host localhost --port %POSTGRES_PORT% --username "%POSTGRES_USER%" --dbname %POSTGRES_DB% --jobs 2 --verbose --exit-on-error %folder%\%fname%
	echo start  : %startTime%
	echo finish : %time%

goto:EOF


:find_extracted_dbs
	set db_tmp_folder=%~1
	echo db_tmp_folder="%db_tmp_folder%"
	pushd "%db_tmp_folder%"
	echo %CD%
	for %%f in (*.bak) do (
		call :restore_db %db_tmp_folder% %%f
	)
	popd
goto:EOF
	



:main
call :find_latest_postgres_install
call :show_menu_ftp
echo 



:exit_script
echo thats's all folks!



