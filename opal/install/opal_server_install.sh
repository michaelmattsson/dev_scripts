#!/bin/bash
clear
echo "================================="
echo "==== exaweb development setup ==="
echo "================================="

cwd=$(pwd)
parentdir="$(dirname "$(pwd)")"
parentdir="$(dirname "$parentdir")"

MY_DEV_ROOT=$(echo $MY_DEV_ROOT | sed 's|\\|/|g')
MY_DEV_ROOT=$(echo $MY_DEV_ROOT | sed 's|C:|/|g')

GIT_REPO_NAME=$(basename "$GIT_REPO_EXAWEB_FORK" .git)
CUR_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
#TMP_DIR=$(mktemp -d)


#########################################################
case "$(uname -s)" in

   Darwin)
     PLATFORM_OS='OSX'
     ;;

   Linux)
     PLATFORM_OS='LINUX'
     ;;

   CYGWIN*|MINGW32*|MINGW64*|MSYS*)
     PLATFORM_OS='WINDOWS'
     ;;

   *)
     PLATFORM_OS='UNKNOWN' 
     ;;
esac

echo running on $PLATFORM_OS

case "$PLATFORM_OS" in
	WINDOWS)
	DEV_ROOT=/c/dev/viztek/exa
	mkdir -p $DEV_ROOT
	DEV_WEB_ROOT=$DEV_ROOT/exaweb
	DEV_ROOT_OS="c:\\dev\\viztek\\exa"
	DEV_WEB_ROOT_OS=$DEV_ROOT_OS/exaweb
	;;
	OSX|LINUX|UNKNOWN)
	DEV_ROOT=/c/dev/viztek/exa
	mkdir -p $DEV_ROOT
	DEV_WEB_ROOT=$DEV_ROOT/exaweb
	DEV_ROOT_OS="$HOME/project/exa"
	DEV_WEB_ROOT_OS=$DEV_ROOT_OS/exaweb
	;;
esac

. $parentdir/my_config.cfg


#
# make required dirs
#
if [ ! -d "$DEV_ROOT" ]; then
	echo "creating viztek exa dev folders"
	mkdir -p $DEV_ROOT
fi
cd $DEV_ROOT

if [ ! -d "$MY_TEMP_DIR" ]; then
	echo "creating temp folder"
	mkdir -p $MY_TEMP_DIR
fi

if [ ! -d "$MY_INSTALL" ]; then
	echo "creating install folder"
	mkdir -p $MY_INSTALL
fi

echo "placing temporary scripts into $MY_TEMP_DIR"

#########################################################


#
# wait for input
#
function pause()
{
	read -p "Press [Enter] key to continue..."
}

#
# creates an msi install script
#
function install_msi()
{
	install_file="$1"
	install_args="$2"
	install_file_watch="$3"
	script_name=msi_wrapper.bat
	create_bat_admin_file $script_name
	echo "./$install_file $install_args">> $script_name
	./$script_name
	#wait for installer to finish
	while [ ! -f "$install_file_watch" ]; do sleep 4; done	
	
	. ~/bashrc
	#rm ./$install_file
	#rm ./msi_wrapper.bat
}

#
# creates a skeleton windows batch file that
# has admin priviledges.  we use this as a template
# when creating batch scripts and append other commands
# to it as needed.
#
function create_bat_admin_file()
{
	filename=$1
	echo "@echo off" > $filename
	echo "@cls" >> $filename
	echo "" >> $filename
	echo ":: FORCE script run in admin mode" >> $filename
	echo ">nul 2>&1 \"%SYSTEMROOT%\system32\cacls.exe\" \"%SYSTEMROOT%\system32\config\system\"" >> $filename
	echo "" >> $filename
	echo "if '%errorlevel%' NEQ '0' (" >> $filename
	echo "    echo Requesting Admin access..." >> $filename
	echo "    goto goUAC )" >> $filename
	echo "    goto goADMIN" >> $filename
	echo "" >> $filename
	echo ":goUAC" >> $filename
	echo "    echo Set UAC = CreateObject^(\"Shell.Application\"^) > \"%temp%\getadmin.vbs\"" >> $filename
	echo "    set params = %*:\"=\"\"" >> $filename
	echo "    echo UAC.ShellExecute \"cmd.exe\", \"/c %~s0 %params%\", \"\", \"runas\", 1 >> \"%temp%\getadmin.vbs\"" >> $filename
	echo "    \"%temp%\getadmin.vbs\"" >> $filename
	echo "    if exist \"%temp%\getadmin.vbs\NUL\" del \"%temp%\getadmin.vbs\"" >> $filename
	echo "    exit /B" >> $filename
	echo "" >> $filename
	echo ":goADMIN" >> $filename
	echo "    pushd \"%CD%\"" >> $filename
	echo "    CD /D \"%~dp0\"" >> $filename
	echo "" >> $filename
}


#
#install ms msql server
#
function install_sql_server()
{
	echo "==================================================="

	#cd $MY_TEMP_DIR
	
	case "$PLATFORM_OS" in
		WINDOWS)
			if [ ! -d "/c/Program Files/PostgreSQL/$POSTGRES_MAJ_VERSION/bin" ] && [ ! -d "/var/lib/postgresql" ]; then
				echo "installing postgres"
				curl https://download.microsoft.com/download/c/2/8/c28cc7df-c9d2-453b-9292-ae7d242dfeca/SQLEXPR_x64_ENU.exe -o SQLEXPR_x64_ENU.exe

				script_name=sqlserver_install.bat
				create_bat_admin_file $script_name
				echo "postgresql-$POSTGRES_VERSION-windows-x64.exe --unattendedmodeui minimal --mode unattended --enable-components pgAdmin --serverport $POSTGRES_PORT --superaccount \"$POSTGRES_USER\" --superpassword \"$POSTGRES_PASS\"" >>  $script_name
				echo "" >> $script_name
				if [ "$SERVICE_POSTGRES" == "manual" ]; then
					echo ":: CHANGE SERVICE TO MANUAL STARTUP ">>  $script_name
					echo "sc config postgresql-x64-$POSTGRES_MAJ_VERSION start= demand">>  $script_name
					#echo "net stop postgresql-x64-$POSTGRES_MAJ_VERSION">>  $script_name
				fi
				./$script_name
				
				#wait for installer to finish
				while [ ! -f "/c/Program Files/PostgreSQL/$POSTGRES_MAJ_VERSION/uninstall-postgresql.dat" ]; do sleep 4; done
				
			else
				echo "postgres already installed."
			fi
		;;
		OSX|LINUX|UNKNOWN)
				echo "opal requires windows."
		;;
	esac
}


function download_sql_server()
{

	#
}