::
:: simple backup script for Opal
:: michael.mattsson@konicaminolta.com
::
echo off
cls
echo -- BACKUP DATABASE --

set date_stamp=%DATE:~-4%.%DATE:~7,2%.%DATE:~4,2%
goto:main


:zip_folder
	set source_dir=%CD%\backups\opal\%date_stamp%
	set target_file=%CD%\backups\opal_backup_%date_stamp%.zip

	::Get command-line arguments
	set vbs="%temp%\_.vbs"
	>%vbs% echo Set objArgs = WScript.Arguments
	>>%vbs% echo Set FS = CreateObject^(^"Scripting.FileSystemObject^"^)
	>>%vbs% echo InputFolder = FS.GetAbsolutePathName^(objArgs^(0^)^)
	>>%vbs% echo ZipFile = FS.GetAbsolutePathName^(objArgs^(1^)^)
	::Create empty ZIP file
	>>%vbs% echo CreateObject^(^"Scripting.FileSystemObject^"^).CreateTextFile^(ZipFile, True^).Write ^"PK^" ^& Chr^(5^) ^& Chr^(6^) ^& String^(18, vbNullChar^)
	>>%vbs% echo Set objShell = CreateObject^(^"Shell.Application^"^)
	>>%vbs% echo Set source = objShell.NameSpace^(InputFolder^).Items
	>>%vbs% echo objShell.NameSpace^(ZipFile^).CopyHere^(source^)
	::Required to let the zip command execute
	::If this script randomly fails or the zip file is not complete
	::just increase to more than 2 seconds
	::>>%vbs% echo wScript.Sleep 2000
	cscript //nologo %vbs% "%source_dir%" "%target_file%"
	::if exist %vbs% del /f /q %vbs%
	::if exist "%CD%\backups\opal" rmdir /S /Q "%CD%\backups\opal"
goto:EOF


:backup_db
	set database_name=%~1
	set target_dir=%CD%\backups\opal\%date_stamp%
	set server_name=%COMPUTERNAME%
	set user_name=sa
	set password=1q2w3e4r5t
	set backup_filename=%target_dir%\%database_name%.bak
	set log=%target_dir%\backup_info.txt
	if not exist "%target_dir%\*" (
		md "%target_dir%"
		>%log% echo backup of %server_name% made on %date_stamp% at %TIME%
		>>%log% echo --------------------------------------------
	)
	>>%log% echo *
	>>%log% echo backing up db: %database_name% to %target_dir%
	::set BACKUPFILENAME=%target_dir%\%database_name%-%date_stamp%.bak
	sqlcmd -U %user_name% -P %password% -S %server_name% -d master -Q "BACKUP DATABASE [%database_name%] TO DISK = N'%backup_filename%' WITH INIT , NOUNLOAD , NAME = N'%database_name% backup', NOSKIP , STATS = 10, NOFORMAT"
goto:EOF


:main

call :backup_db opalrad
call :backup_db opalrad_audit
call :backup_db opalrad_tmp
call :backup_db ASPSTATE
::call :zip_folder %date_stamp%
set source_dir=%CD%\backups\opal\%date_stamp%
set target_file=%CD%\backups\opal_backup_%date_stamp%.zip

echo %CD%\7za.exe a -tzip %target_file% %source_dir%
call %CD%\7za.exe a -tzip %target_file% %source_dir%
if exist "%CD%\backups\opal\*" rmdir /S /Q "%CD%\backups\opal"
pause