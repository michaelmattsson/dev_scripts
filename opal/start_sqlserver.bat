::
:: START OPAL
::
@echo off
@cls
:: FORCE script run in admin mode
>nul 2>&1 "%SYSTEMROOT%\system32\cacls.exe" "%SYSTEMROOT%\system32\config\system"

if '%errorlevel%' NEQ '0' (
    echo Requesting Admin access...
    goto goUAC )
    goto goADMIN

:goUAC
    echo Set UAC = CreateObject^("Shell.Application"^) > "%temp%\getadmin.vbs"
    set params = %*:"=""
    echo UAC.ShellExecute "cmd.exe", "/c %~s0 %params%", "", "runas", 1 >> "%temp%\getadmin.vbs"
    "%temp%\getadmin.vbs"
    del "%temp%\getadmin.vbs"
    exit /B

:goADMIN
    pushd "%CD%"
    CD /D "%~dp0"

set "common_scripts_dir=../common"

::get status of IIS, put then in WAS & W3SVC variables
setlocal enabledelayedexpansion
for /f "delims=" %%a in ('iisreset /status') do (
 set "var=%%a"
 set "var=!var:*( =!"
   for /f "tokens=1,2 delims=): " %%b in ("!var!") do (
      set "%%b=%%c"
   )
)
set started=false

if "%WAS%"=="Running" set started="true"
if "%W3SVC%"=="Running" set started="true"

if "%started%"=="false" (iisreset /start) else (iisreset /stop)

call %common_scripts_dir%/tasks.cmd :START_WIN_SERVICE "MSSQLSERVER" "MSSQLSERVER"
call %common_scripts_dir%/tasks.cmd :START_WIN_SERVICE "SQLWriter" "SQLWriter"

