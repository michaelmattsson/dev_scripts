::
:: STOP OPAL
::
@echo off
@cls
:: FORCE script run in admin mode
>nul 2>&1 "%SYSTEMROOT%\system32\cacls.exe" "%SYSTEMROOT%\system32\config\system"

if '%errorlevel%' NEQ '0' (
    echo Requesting Admin access...
    goto goUAC )
    goto goADMIN

:goUAC
    echo Set UAC = CreateObject^("Shell.Application"^) > "%temp%\getadmin.vbs"
    set params = %*:"=""
    echo UAC.ShellExecute "cmd.exe", "/c %~s0 %params%", "", "runas", 1 >> "%temp%\getadmin.vbs"
    "%temp%\getadmin.vbs"
    del "%temp%\getadmin.vbs"
    exit /B

:goADMIN
    pushd "%CD%"
    CD /D "%~dp0"

set common_scripts_dir=../common
iisreset /stop
call %common_scripts_dir%/tasks.cmd :STOP_WIN_SERVICE "MSSQLSERVER" "MSSQLSERVER.exe"
call %common_scripts_dir%/tasks.cmd :STOP_WIN_SERVICE "SQLWriter" "SQLWriter.exe"
call %common_scripts_dir%/tasks.cmd :STOP_WIN_SERVICE "Opal Agent" "OpalAgent"
call %common_scripts_dir%/tasks.cmd :STOP_WIN_SERVICE "Opal Backup" "OpalBU"
call %common_scripts_dir%/tasks.cmd :STOP_WIN_SERVICE "OpalRAD DICOM Print" "OpalDicomPrint"
call %common_scripts_dir%/tasks.cmd :STOP_WIN_SERVICE "OpalRAD DICOM Receive" "OpalDICOMReceive"
call %common_scripts_dir%/tasks.cmd :STOP_WIN_SERVICE "OpalRAD Listener" "OpalListener"
call %common_scripts_dir%/tasks.cmd :STOP_WIN_SERVICE "OpalRAD Router" "OpalRouter"
call %common_scripts_dir%/tasks.cmd :STOP_WIN_SERVICE "OpalRAD ImageServer" "OpalImageServer"



